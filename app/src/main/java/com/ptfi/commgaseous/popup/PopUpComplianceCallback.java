package com.ptfi.commgaseous.popup;

import com.ptfi.commgaseous.model.ComplianceModel;

/**
 * Created by GO on 7/18/2016.
 */
public interface PopUpComplianceCallback {
    public void onDataSave(ComplianceModel model);
}
