package com.ptfi.commgaseous.popup;

import android.graphics.Bitmap;

import com.ptfi.commgaseous.model.ComplianceModel;

/**
 * Created by GO on 7/18/2016.
 */
public interface GestureCallback {
    public void onBitmapSaved(Bitmap bitmap);
}
