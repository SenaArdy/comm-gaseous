package com.ptfi.commgaseous.popup;

import com.ptfi.commgaseous.model.GaseousModel;
import com.ptfi.commgaseous.model.HandoverModel;

/**
 * Created by senaardyputra on 7/27/16.
 */
public interface PopUpCallbackInspector {
    public void onDataSave(GaseousModel model, String name, String division);

    public void onDataSaveHO(HandoverModel model, String name, String division);
}
