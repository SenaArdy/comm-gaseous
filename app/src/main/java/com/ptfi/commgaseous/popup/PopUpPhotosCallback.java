package com.ptfi.commgaseous.popup;

import com.ptfi.commgaseous.model.PhotoModel;

/**
 * Created by GO on 8/15/2016.
 */
public interface PopUpPhotosCallback {
    public void onDataSave(PhotoModel model);
}
