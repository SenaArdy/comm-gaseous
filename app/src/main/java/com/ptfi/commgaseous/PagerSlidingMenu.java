package com.ptfi.commgaseous;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.ptfi.commgaseous.fragment.GaseousFragment;
import com.ptfi.commgaseous.fragment.GeneratedDataFragment;
import com.ptfi.commgaseous.fragment.HandoverFragment;
import com.ptfi.commgaseous.fragment.ManageDataFragment;
import com.ptfi.commgaseous.fragment.QuickContactFragment;
import com.ptfi.commgaseous.popup.PopUp;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by GO on 7/18/2016.
 */
public class PagerSlidingMenu extends ActionBarActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.tabs)
    PagerSlidingTabStrip tabs;
    @InjectView(R.id.pager)
    ViewPager pager;

    private MyPagerAdapter adapter;
    private Drawable oldBackground = null;
    private int currentColor;
    private SystemBarTintManager mTintManager;

    static String registerEQ;
    static String locationEQ;
    static String typeEQ;
    static String lastDateEQ;
    static String nextDateEQ;
    static ImageView informationApp;

    GaseousFragment gaseousFragment;
    HandoverFragment handoverFragment;
    ManageDataFragment manageDataFragment;
    GeneratedDataFragment generatedDataFragment;
    int currentPage = 0;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_sliding);

//sliding tab
        ButterKnife.inject(PagerSlidingMenu.this);
        setSupportActionBar(toolbar);
// create our manager instance after the content view is set
        mTintManager = new SystemBarTintManager(this);
//        enable status bar tint
        mTintManager.setStatusBarTintEnabled(true);

//        initalize fragment
        gaseousFragment = new GaseousFragment();
        handoverFragment = new HandoverFragment();
        manageDataFragment = new ManageDataFragment();
        generatedDataFragment = new GeneratedDataFragment();

        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(2);
        tabs.setViewPager(pager);
        tabs.setIndicatorColor(getResources().getColor(R.color.White));
        tabs.setIndicatorHeight(5);
        tabs.setTextColor(getResources().getColor(R.color.White));
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        pager.setCurrentItem(0);
        changeColor(getResources().getColor(R.color.colorRedAccent));

        tabs.setOnTabReselectedListener(new PagerSlidingTabStrip.OnTabReselectedListener() {
            @Override
            public void onTabReselected(int position) {
                if (position == 0) {
                    Toast.makeText(PagerSlidingMenu.this, "Tab reselected: " + "Commissioning Gaseous", Toast.LENGTH_SHORT).show();
                } else if (position == 1) {
                    Toast.makeText(PagerSlidingMenu.this, "Tab reselected: " + "Handover", Toast.LENGTH_SHORT).show();
                } else if (position == 2) {
                    Toast.makeText(PagerSlidingMenu.this, "Tab reselected: " + "All Generated Data", Toast.LENGTH_SHORT).show();
                } else  {
                    Toast.makeText(PagerSlidingMenu.this, "Tab reselected: " + "Data Management", Toast.LENGTH_SHORT).show();
                }
            }
        });
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        informationApp = (ImageView) findViewById(R.id.informationApp);
        informationApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuickContactFragment.newInstance().show(getFragmentManager(), "QuickContactFragment");
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        customActionBar();
        return true;
    }

    private void customActionBar() {
        LayoutInflater customBar = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = customBar.inflate(R.layout.activity_all_pages, null);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        actionBar.setCustomView(actionBarView);
    }
//    public boolean onOptionsItemSelected (MenuItem item){
//        switch (item.getItemId()){
//            case R.id.action_contact2:
//                QuickContactFragment.newInstance().show(getFragmentManager(), "QuickContactFragment");
//                return true;
//
//            case R.id.action_contact:
//                QuickContactFragment.newInstance().show(getFragmentManager(), "QuickContactFragment");
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//    public void aboutChangelog(View v){
//    }

    private void changeColor(int newColor) {
        tabs.setBackgroundColor(newColor);
        mTintManager.setTintColor(newColor);
        // change ActionBar color just if an ActionBar is available
        Drawable colorDrawable = new ColorDrawable(newColor);
        Drawable bottomDrawable = new ColorDrawable(getResources().getColor(android.R.color.transparent));
        LayerDrawable ld = new LayerDrawable(new Drawable[]{colorDrawable, bottomDrawable});
        if (oldBackground == null) {
            getSupportActionBar().setBackgroundDrawable(ld);
        } else {
            TransitionDrawable td = new TransitionDrawable(new Drawable[]{oldBackground, ld});
            getSupportActionBar().setBackgroundDrawable(td);
            td.startTransition(200);
        }

        oldBackground = ld;
        currentColor = newColor;
    }

    public class MyPagerAdapter extends FragmentPagerAdapter {

        private final String[] TITLES = {"Commissioning", "Handover", "All Generated Data", "Data Management"};
        private Fragment[] fragment = new Fragment[] {gaseousFragment,
                handoverFragment, generatedDataFragment, manageDataFragment};

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return TITLES[position];
        }

        @Override
        public int getCount() {
            return TITLES.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0 :
                    return gaseousFragment;

                case 1 :
                    return handoverFragment;

                case 2 :
                    return generatedDataFragment;

                case 3 :
                    return manageDataFragment;
            }
//           return fragment[position];
            return null;
        }

    }

    public void datePicker(final View v){
        GaseousFragment.datePickers(PagerSlidingMenu.this, v);
    }

    public void datePickerHandover(final View v){
        HandoverFragment.datePickersHandover(PagerSlidingMenu.this, v);
    }

    public void datePickersGenerated(final View v){
        GeneratedDataFragment.datePickersGenerated(PagerSlidingMenu.this, v);
    }

    @Override
    public void onBackPressed() {
        switch (pager.getCurrentItem()) {
            case 0 :
                GaseousFragment.onBackPressed(this);
                break;

            case 1 :
                HandoverFragment.onBackPressed(this);
                break;

            case 2 :
                GeneratedDataFragment.onBackPressed(this);
                break;

            case 3 :
                ManageDataFragment.onBackPressed(this);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (currentPage == 0) {
            if (gaseousFragment.isVisible()) {
                gaseousFragment.onActivityResult(requestCode, resultCode, data);
                PopUp.pageComm = PopUp.page.commissioning;
            }
        } else if (currentPage == 1) {
            if (handoverFragment.isVisible()) {
                handoverFragment.onActivityResult(requestCode, resultCode,data);
                PopUp.pageComm = PopUp.page.handover;
            }
        }  else if (currentPage == 3) {
            if (manageDataFragment.isVisible()) {
                manageDataFragment.onActivityResult(requestCode, resultCode,data);
                PopUp.pageComm = PopUp.page.manageData;
            }
        } else if (currentPage == 2) {
            if (generatedDataFragment.isVisible()) {
                generatedDataFragment.onActivityResult(requestCode, resultCode,data);
                PopUp.pageComm = PopUp.page.generateData;
            }
        }
    }

}
