package com.ptfi.commgaseous.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ptfi.commgaseous.R;
import com.ptfi.commgaseous.model.PhotoModel;
import com.ptfi.commgaseous.popup.PhotoPopUp;
import com.ptfi.commgaseous.utility.Helper;

import java.util.ArrayList;

/**
 * Created by GO on 8/15/2016.
 */
public class ListPhotoAdapter extends RecyclerView.Adapter<ListPhotoAdapter.dataViewHolder> {

    private static Activity mActivity;
    private static ArrayList<PhotoModel> photoData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        ImageView photoIV;
        TextView titleTV;
        TextView commentTV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.photoIV = (ImageView) itemView.findViewById(R.id.photoIV);
            this.titleTV = (TextView) itemView.findViewById(R.id.titleTV);
            this.commentTV = (TextView) itemView.findViewById(R.id.commentTV);

            listCV.setOnClickListener(this);
            photoIV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String equipment = photoData.get(position).getEquipment();
            String date = photoData.get(position).getDate();
            String register = photoData.get(position).getRegister();
            String pathFile = photoData.get(position).getPathFile();
            String title = photoData.get(position).getTitle();
            String comment = photoData.get(position).getComment();

            switch (v.getId()) {
                case R.id.photoIV:
                    PhotoPopUp.showPopupPreview(mActivity, pathFile);
                    break;

                case R.id.listCV:
                    PhotoPopUp.showImagePopUp(mActivity, equipment, date, register, pathFile, title, comment);
                    break;

                default:
                    break;
            }

//        PopUp.openPreviousData(mActivity, register, location, type, lastDate, nextDate, inspector, inspectorID);
        }
    }

    public ListPhotoAdapter(Activity mActivity, ArrayList<PhotoModel> photoData) {
        super();
        this.mActivity = mActivity;
        this.photoData = photoData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_photo_data, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        ImageView photoIV = holder.photoIV;
        TextView titleTV = holder.titleTV;
        TextView commentTV = holder.commentTV;

        Helper.setPic(photoIV, photoData.get(position).getPathFile());
        titleTV.setText(photoData.get(position).getTitle());
        commentTV.setText(photoData.get(position).getComment());
    }

    @Override
    public int getItemCount() {
        return photoData.size();
    }
}
