package com.ptfi.commgaseous.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ptfi.commgaseous.R;
import com.ptfi.commgaseous.model.ComplianceModel;

import java.util.ArrayList;

/**
 * Created by GO on 7/18/2016.
 */
public class ListComplianceAdapter extends RecyclerView.Adapter<ListComplianceAdapter.dataViewHolder>{

    private static Activity mActivity;
    private static ArrayList<ComplianceModel> complianceData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        TextView findingsTV;
        TextView remarkTV;
        TextView responsibilityTV;
        TextView statusTV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.findingsTV = (TextView) itemView.findViewById(R.id.findingsTV);
            this.remarkTV = (TextView) itemView.findViewById(R.id.remarkTV);
            this.responsibilityTV = (TextView) itemView.findViewById(R.id.responsibilityTV);
            this.statusTV = (TextView) itemView.findViewById(R.id.statusTV);

            listCV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String findings = complianceData.get(position).getFindings();
            String remark = complianceData.get(position).getRemark();
            String responsibility = complianceData.get(position).getResponsibility();
            String status = complianceData.get(position).getDone();

//        PopUp.openPreviousData(mActivity, register, location, type, lastDate, nextDate, inspector, inspectorID);
        }
    }

    public ListComplianceAdapter(Activity mActivity, ArrayList<ComplianceModel> complianceData) {
        super();
        this.mActivity = mActivity;
        this.complianceData = complianceData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_compliance_data, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }


    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        TextView findingsTV = holder.findingsTV;
        TextView remarkTV = holder.remarkTV;
        TextView responsibilityTV = holder.responsibilityTV;
        TextView statusTV = holder.statusTV;

        findingsTV.setText(complianceData.get(position).getFindings());
        remarkTV.setText(complianceData.get(position).getRemark());
        responsibilityTV.setText("Responsibility : " + complianceData.get(position).getResponsibility());
        statusTV.setText("Status : " + complianceData.get(position).getDone());
//        if (complianceData.get(position).getLastInspection() != null) {
//            if (Helper.countDays(complianceData.get(position).getLastInspection()) > 11) {
//                holder.nextDateTV.setTextColor(mActivity.getResources().getColor(R.color.colorRedAccent));
//            } else if (Helper.countDays(complianceData.get(position).getLastInspection()) > 7) {
//                holder.nextDateTV.setTextColor(mActivity.getResources().getColor(R.color.colorYellowAccent));
//            }else {
//                holder.nextDateTV.setTextColor(mActivity.getResources().getColor(R.color.GrayLight));
//            }
//        } else {
//            holder.nextDateTV.setTextColor(mActivity.getResources().getColor(R.color.GrayLight));
//        }
    }

    @Override
    public int getItemCount() {
        return complianceData.size();
    }
}
