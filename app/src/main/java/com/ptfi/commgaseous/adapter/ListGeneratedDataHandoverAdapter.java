package com.ptfi.commgaseous.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ptfi.commgaseous.R;
import com.ptfi.commgaseous.model.HandoverModel;
import com.ptfi.commgaseous.popup.GeneratePopUp;

import java.util.ArrayList;

/**
 * Created by GO on 8/25/2016.
 */
public class ListGeneratedDataHandoverAdapter extends RecyclerView.Adapter<ListGeneratedDataHandoverAdapter.dataViewHolder>{

    private static Activity mActivity;
    private static ArrayList<HandoverModel> handOverData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        TextView equipmentTV;
        TextView locationTV;
        TextView dateTV;
        TextView registerTV;
        TextView typeTV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.equipmentTV = (TextView) itemView.findViewById(R.id.equipmentTV);
            this.locationTV = (TextView) itemView.findViewById(R.id.locationEQTV);
            this.dateTV = (TextView) itemView.findViewById(R.id.dateTV);
            this.registerTV = (TextView) itemView.findViewById(R.id.registerTV);
            this.typeTV = (TextView) itemView.findViewById(R.id.typeTV);

            listCV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            String equipment = handOverData.get(position).getEquipment();
            String date = handOverData.get(position).getDate();
            String location = handOverData.get(position).getLocation();
            String type = handOverData.get(position).getType();
            String register = handOverData.get(position).getRegister();

            GeneratePopUp.handOverPopUp(mActivity, equipment, date, location, type, register);
        }
    }

    public ListGeneratedDataHandoverAdapter(Activity mActivity, ArrayList<HandoverModel> handOverData) {
        super();
        this.mActivity = mActivity;
        this.handOverData = handOverData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_compliance_data, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        CardView listCV = holder.listCV;
        TextView equipmentTV = holder.equipmentTV;
        TextView locationTV = holder.locationTV;
        TextView dateTV = holder.dateTV;
        TextView registerTV = holder.registerTV;
        TextView typeTV = holder.typeTV;

        equipmentTV.setText(handOverData.get(position).getEquipment());
        locationTV.setText("Equipment Location : " + handOverData.get(position).getLocation());
        dateTV.setText("Inspection Date : " + handOverData.get(position).getDate());
        typeTV.setText("Equipment Type : " + handOverData.get(position).getType());
        registerTV.setText("Equipment Register : " + handOverData.get(position).getRegister());
    }

    @Override
    public int getItemCount() {
        return handOverData.size();
    }

}
