package com.ptfi.commgaseous.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ptfi.commgaseous.R;
import com.ptfi.commgaseous.model.TermsModel;

import java.util.ArrayList;

/**
 * Created by GO on 7/18/2016.
 */
public class ListTermsAdapter extends RecyclerView.Adapter<ListTermsAdapter.dataViewHolder> {

    private static Activity mActivity;
    private static ArrayList<TermsModel> termsData;

    public static class dataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CardView listCV;
        TextView idTV;
        TextView termsTV;

        public dataViewHolder(View itemView) {
            super(itemView);
            listCV = (CardView) itemView.findViewById(R.id.listCV);
            this.idTV = (TextView) itemView.findViewById(R.id.idTV);
            this.termsTV = (TextView) itemView.findViewById(R.id.termsTV);

            listCV.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
//            String findings = complianceData.get(position).getFindings();
//            String remark = complianceData.get(position).getRemark();
//            String responsibility = complianceData.get(position).getResponsibility();
//            String status = complianceData.get(position).getDone();

//        PopUp.openPreviousData(mActivity, register, location, type, lastDate, nextDate, inspector, inspectorID);
        }
    }

    public ListTermsAdapter(Activity mActivity, ArrayList<TermsModel> termsDataData) {
        super();
        this.mActivity = mActivity;
        this.termsData = termsDataData;
    }

    @Override
    public dataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_terms_data, parent, false);
        dataViewHolder dataViewHolder = new dataViewHolder(view);
        return dataViewHolder;
    }

    @Override
    public void onBindViewHolder(dataViewHolder holder, int position) {
        TextView idTV = holder.idTV;
        TextView termsTV = holder.termsTV;

        idTV.setText(Integer.toString(position + 1) + ".");
        termsTV.setText(termsData.get(position).getTerms());
//        if (complianceData.get(position).getLastInspection() != null) {
//            if (Helper.countDays(complianceData.get(position).getLastInspection()) > 11) {
//                holder.nextDateTV.setTextColor(mActivity.getResources().getColor(R.color.colorRedAccent));
//            } else if (Helper.countDays(complianceData.get(position).getLastInspection()) > 7) {
//                holder.nextDateTV.setTextColor(mActivity.getResources().getColor(R.color.colorYellowAccent));
//            }else {
//                holder.nextDateTV.setTextColor(mActivity.getResources().getColor(R.color.GrayLight));
//            }
//        } else {
//            holder.nextDateTV.setTextColor(mActivity.getResources().getColor(R.color.GrayLight));
//        }
    }

    @Override
    public int getItemCount() {
        return termsData.size();
    }
}
