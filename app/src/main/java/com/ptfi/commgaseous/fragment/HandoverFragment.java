package com.ptfi.commgaseous.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.CaptureActivity;
import com.ptfi.commgaseous.R;
import com.ptfi.commgaseous.adapter.ListTermsAdapter;
import com.ptfi.commgaseous.database.DataSource;
import com.ptfi.commgaseous.model.DataSingleton;
import com.ptfi.commgaseous.model.GaseousModel;
import com.ptfi.commgaseous.model.HandoverModel;
import com.ptfi.commgaseous.model.InspectorModel;
import com.ptfi.commgaseous.model.TermsModel;
import com.ptfi.commgaseous.popup.PhotoPopUp;
import com.ptfi.commgaseous.popup.PopUp;
import com.ptfi.commgaseous.popup.PopUpCallbackInspector;
import com.ptfi.commgaseous.utility.FoldersFilesName;
import com.ptfi.commgaseous.utility.Helper;
import com.ptfi.commgaseous.utility.Reports;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by GO on 7/18/2016.
 */
public class HandoverFragment extends Fragment {

    protected static FragmentActivity mActivity;

    static final String ACTION_SCAN = "com.google.zxing.client.android.SCAN";
    static final int REQUEST_SCAN_BARCODE_CLIENT = 347;
    static final int REQUEST_SCAN_BARCODE_ENG = 348;
    static final int REQUEST_SCAN_BARCODE_MAIN = 349;
    static final int REQUEST_SCAN_BARCODE_AO = 350;
    static final int REQUEST_SCAN_BARCODE_MAINRES = 351;
    static final int REQUEST_SCAN_BARCODE_CSE = 352;
    static final int REQUEST_SCAN_BARCODE_DEPT = 353;

    static String inspectorIDFalse;

    ArrayList<InspectorModel> dataInspector;

    private PopUp.page pages;
    private PopUp.signature signatures;

    static EditText equipmentET, dateET, clientET, typeET, registerET, locationET, engineeringET, maintenanceET,
            areaOwnerET, ugmrET, cseET, deptHeadET, addTermsET;

    static TextInputLayout equipmentTL, dateTL, clientTL, typeTL, registerTL, locationTL, engineeringTL, maintenanceTL,
            areaOwnerTL, ugmrTL, cseTL, deptHeadTL, addTermsTL;

    static LinearLayout termsLL;

    static RecyclerView termsRV;

    TextView imeiCode, versionCode;

    static CardView generateCV, previewCV, addCV;

    RecyclerView.LayoutManager layoutManager;

    private static ArrayList<TermsModel> termsData = new ArrayList<>();

    ListTermsAdapter adapterData;

    private static int selectedYear = 0;
    private static int selectedMonth = 0;
    private static int selectedDate = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_handover, container, false);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        PopUp.pageComm = PopUp.page.handover;

        versionCode = (TextView) rootView.findViewById(R.id.versionCode);
        PackageInfo pInfo;
        try {
            pInfo = mActivity.getPackageManager().getPackageInfo(mActivity.getPackageName(), 0);
            String version = pInfo.versionName;
            versionCode.setText("Version : " + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        // Imei
        TelephonyManager telephonyManager = (TelephonyManager) mActivity.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceID = telephonyManager.getDeviceId();
        imeiCode = (TextView) rootView.findViewById(R.id.imeiCode);
        imeiCode.setText("Device IMEI : " + deviceID);

        initComponent(rootView);

        return rootView;
    }

    public void initComponent(final View rootView) {
        if (rootView != null) {
            equipmentET = (EditText) rootView.findViewById(R.id.equipmentET);
            registerET = (EditText) rootView.findViewById(R.id.registerET);
            dateET = (EditText) rootView.findViewById(R.id.dateET);
            clientET = (EditText) rootView.findViewById(R.id.clientET);
            typeET = (EditText) rootView.findViewById(R.id.typeEqET);
            locationET = (EditText) rootView.findViewById(R.id.locationEqET);
            addTermsET = (EditText) rootView.findViewById(R.id.addTermsET);

            equipmentTL = (TextInputLayout) rootView.findViewById(R.id.equipmentTL);
            registerTL = (TextInputLayout) rootView.findViewById(R.id.registerTL);
            dateTL = (TextInputLayout) rootView.findViewById(R.id.dateTL);
            clientTL = (TextInputLayout) rootView.findViewById(R.id.clientTL);
            typeTL = (TextInputLayout) rootView.findViewById(R.id.typeEqTL);
            locationTL = (TextInputLayout) rootView.findViewById(R.id.locationEqTL);
            addTermsTL = (TextInputLayout) rootView.findViewById(R.id.addTermsTL);

            engineeringET = (EditText) rootView.findViewById(R.id.engineeringET);
            maintenanceET = (EditText) rootView.findViewById(R.id.maintenanceET);
            areaOwnerET = (EditText) rootView.findViewById(R.id.areaOwnerET);
            ugmrET = (EditText) rootView.findViewById(R.id.ugmrET);
            cseET = (EditText) rootView.findViewById(R.id.cseET);
            deptHeadET = (EditText) rootView.findViewById(R.id.deptHeadET);

            engineeringTL = (TextInputLayout) rootView.findViewById(R.id.engineeringTL);
            maintenanceTL = (TextInputLayout) rootView.findViewById(R.id.maintenanceTL);
            areaOwnerTL = (TextInputLayout) rootView.findViewById(R.id.areaOwnerTL);
            ugmrTL = (TextInputLayout) rootView.findViewById(R.id.ugmrTL);
            cseTL = (TextInputLayout) rootView.findViewById(R.id.cseTL);
            deptHeadTL = (TextInputLayout) rootView.findViewById(R.id.deptHeadTL);

            termsLL = (LinearLayout) rootView.findViewById(R.id.termsLL);

            termsRV = (RecyclerView) rootView.findViewById(R.id.termsRV);

            addCV = (CardView) rootView.findViewById(R.id.addCV);
            addCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (addTermsET.getText().toString().length() >= 1) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();

                        TermsModel model = new TermsModel();

                        model.setTerms(addTermsET.getText().toString());
                        model.setEquipment(equipmentET.getText().toString());
                        model.setDate(dateET.getText().toString());
                        model.setRegister(registerET.getText().toString());

                        ds.insertTerms(model);
                        findingRv();

                        ds.close();

                        addTermsET.setText("");
                    } else if (addTermsET.getText().toString().length() == 0) {
                        addTermsTL.setErrorEnabled(true);
                        addTermsTL.setError("*Please fill this Terms and Conditions");
                    }
                }
            });
            generateCV = (CardView) rootView.findViewById(R.id.generateCV);
            previewCV = (CardView) rootView.findViewById(R.id.previewCV);

            generateCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataSource ds = new DataSource(mActivity);
                    ds.open();
                    if (validationGenerate()) {
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            Reports.createHandOverReport(mActivity, equipmentET.getText().toString(), dateET.getText().toString(),
                                    locationET.getText().toString(), typeET.getText().toString(), registerET.getText().toString(),
                                    true);
                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            Reports.createHandOverReport(mActivity, equipmentET.getText().toString(), dateET.getText().toString(),
                                    locationET.getText().toString(), typeET.getText().toString(), registerET.getText().toString(),
                                    true);
                        }
                    }
                    ds.open();
                }
            });

            previewCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataSource ds = new DataSource(mActivity);
                    ds.open();
                    if(validationGenerate()) {
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            Reports.createHandOverReport(mActivity, equipmentET.getText().toString(), dateET.getText().toString(),
                                    locationET.getText().toString(), typeET.getText().toString(), registerET.getText().toString(),
                                    false);
                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            Reports.createHandOverReport(mActivity, equipmentET.getText().toString(), dateET.getText().toString(),
                                    locationET.getText().toString(), typeET.getText().toString(), registerET.getText().toString(),
                                    false);
                        }
                    }
                    ds.close();
                }
            });

            engineeringET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.engineering;
                            scanBar(signatures);
                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.engineering;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            maintenanceET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.maintenance;
                            scanBar(signatures);
                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.maintenance;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            areaOwnerET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        final DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(GaseousModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateAOHandOver(model);
                                            areaOwnerET.setText(name);

                                        }
                                    });

                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.areaOwner;
                            PopUp.showInspectorPopUp(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(GaseousModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateAOHandOver(model);
                                            areaOwnerET.setText(name);

                                        }
                                    });
                        }
                    }
                }
            });

            ugmrET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    if (validation()) {
                        final DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(GaseousModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateMainResHandOver(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);
                                        }
                                    });

                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.mainRes;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(GaseousModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateMainResHandOver(model);
                                            ugmrTL.setHint(division);
                                            ugmrET.setText(name);
                                        }
                                    });
                        }
                    }
                }
            });

            cseET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        final DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(GaseousModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateCSEHandOver(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);
                                        }
                                    });

                        } else  if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            PopUp.page pages = PopUp.page.handover;
                            PopUp.signature signatures = PopUp.signature.CSE;
                            PopUp.showInspectorPopUpDivision(mActivity, null, null, pages, signatures, equipmentET.getText().toString(),
                                    dateET.getText().toString(), registerET.getText().toString(), new PopUpCallbackInspector() {
                                        @Override
                                        public void onDataSave(GaseousModel model, String name, String division) {

                                        }

                                        @Override
                                        public void onDataSaveHO(HandoverModel model, String name, String division) {
                                            ds.updateCSEHandOver(model);
                                            cseTL.setHint(division);
                                            cseET.setText(name);
                                        }
                                    });
                        }
                    }
                }
            });

            deptHeadET.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validation()) {
                        DataSource ds = new DataSource(mActivity);
                        ds.open();
                        if (!ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            saveData();
                            signatures = PopUp.signature.deptHead;
                            scanBar(signatures);
                        } else if (ds.validateDataHandOver(equipmentET.getText().toString(),
                                dateET.getText().toString(), registerET.getText().toString())) {
                            updateData();
                            signatures = PopUp.signature.deptHead;
                            scanBar(signatures);
                        }
                        ds.close();
                    }
                }
            });

            // Date
            final Calendar c = Calendar.getInstance();
            selectedYear = c.get(Calendar.YEAR);
            selectedMonth = c.get(Calendar.MONTH);
            selectedDate = c.get(Calendar.DAY_OF_MONTH);
            DataSingleton.getInstance().setDate(selectedYear, selectedMonth,
                    selectedDate);
            dateET.setText(DataSingleton.getInstance().getFormattedDate());
        }
    }

    public static boolean validation() {
        boolean status = true;
        String emptyFields = "";

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Sprinkler Equipment");
            emptyFields += "Please fill Sprinkler Equipment\n";
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
            emptyFields += "Please fill Date Inspection\n";
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Sprinkler Location");
            emptyFields += "Please fill Sprinkler Location\n";
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Sprinkler Type");
            emptyFields += "Please fill Sprinkler Type\n";
        }

        if (clientET.getText().toString().length() == 0) {
            status = false;
            clientTL.setErrorEnabled(true);
            clientTL.setError("*Please fill Client Data");
            emptyFields += "Please fill Client Data\n";
        }

        if (!status) {
            Helper.showPopUpMessage(mActivity, "Warning",
                    "Please fill the empty field(s):\n" + emptyFields, null);
        }

        return status;
    }


    public static boolean validationGenerate() {
        boolean status = true;
        String emptyFields = "";

        if (equipmentET.getText().toString().length() == 0) {
            status = false;
            equipmentTL.setErrorEnabled(true);
            equipmentTL.setError("*Please fill Sprinkler Equipment");
            emptyFields += "Please fill Sprinkler Equipment\n";
        }

        if (dateET.getText().toString().length() == 0) {
            status = false;
            dateTL.setErrorEnabled(true);
            dateTL.setError("*Please fill Date Inspection");
            emptyFields += "Please fill Date Inspection\n";
        }

        if (locationET.getText().toString().length() == 0) {
            status = false;
            locationTL.setErrorEnabled(true);
            locationTL.setError("*Please fill Sprinkler Location");
            emptyFields += "Please fill Sprinkler Location\n";
        }

        if (typeET.getText().toString().length() == 0) {
            status = false;
            typeTL.setErrorEnabled(true);
            typeTL.setError("*Please fill Sprinkler Type");
            emptyFields += "Please fill Sprinkler Type\n";
        }

        if (clientET.getText().toString().length() == 0) {
            status = false;
            clientTL.setErrorEnabled(true);
            clientTL.setError("*Please fill Client Data");
            emptyFields += "Please fill Client Data\n";
        }

        if (engineeringET.getText().toString().length() == 0) {
            status = false;
            engineeringTL.setErrorEnabled(true);
            engineeringTL.setError("*Please fill Engineering Name");
            emptyFields += "Please fill Engineering Name\n";
        }

//        if (maintenanceET.getText().toString().length() == 0) {
//            status = false;
//            maintenanceTL.setErrorEnabled(true);
//            maintenanceTL.setError("*Please fill Maintenance Name");
//            emptyFields += "Please fill Maintenance Name\n";
//        }

//        if (areaOwnerET.getText().toString().length() == 0) {
//            status = false;
//            areaOwnerTL.setErrorEnabled(true);
//            areaOwnerTL.setError("*Please fill Area Owner Name");
//            emptyFields += "Please fill Area Owner Name\n";
//        }

//        if (ugmrET.getText().toString().length() == 0) {
//            status = false;
//            ugmrTL.setErrorEnabled(true);
//            ugmrTL.setError("*Please fill UGMR Name");
//            emptyFields += "Please fill UGMR Name\n";
//        }

//        if (cseET.getText().toString().length() == 0) {
//            status = false;
//            cseTL.setErrorEnabled(true);
//            cseTL.setError("*Please fill CSE Name");
//            emptyFields += "Please fill CSE Name\n";
//        }

//        if (deptHeadET.getText().toString().length() == 0) {
//            status = false;
//            deptHeadTL.setErrorEnabled(true);
//            deptHeadTL.setError("*Please fill Department Head Name");
//            emptyFields += "Please fill Department Head Name\n";
//        }

        if (!status) {
            Helper.showPopUpMessage(mActivity, "Warning",
                    "Please fill the empty field(s):\n" + emptyFields, null);
        }

        return status;
    }

    public void findingRv() {
        termsRV.setHasFixedSize(true);
        DataSource ds = new DataSource(mActivity);
        ds.open();
        layoutManager = new LinearLayoutManager(getActivity());
        termsRV.setLayoutManager(layoutManager);
        termsRV.setItemAnimator(new DefaultItemAnimator());

        termsData = ds.getAllTermsEquipment(equipmentET.getText().toString(),
                dateET.getText().toString(), registerET.getText().toString());

        adapterData = new ListTermsAdapter(mActivity, termsData);
        termsRV.setAdapter(adapterData);
        ds.close();
    }

    public static void datePickersHandover(final Activity mActivity, final View rootView) {
        Helper.showDatePicker(rootView, (FragmentActivity) mActivity,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        selectedYear = year;
                        selectedMonth = monthOfYear;
                        selectedDate = dayOfMonth;

                        DataSingleton.getInstance().setDate(year, monthOfYear, dayOfMonth);
                        ((EditText) rootView).setText(DataSingleton.getInstance()
                                .getFormattedDate());
                    }
                }, selectedYear, selectedMonth, selectedDate);
    }

    private static android.app.AlertDialog showQRDialog(final Activity act,
                                                        CharSequence title, CharSequence message, CharSequence buttonYes,
                                                        CharSequence buttonNo) {
        android.app.AlertDialog.Builder downloadDialog = new android.app.AlertDialog.Builder(act);
        downloadDialog.setTitle(title);
        downloadDialog.setMessage(message);
        downloadDialog.setPositiveButton(buttonYes,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Helper.copyAndOpenBarcodeScannerAPK(act);
                    }
                });
        downloadDialog.setNegativeButton(buttonNo,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
        return downloadDialog.show();
    }

    public static void scanBar(PopUp.signature signatures) {
        try {
            Intent intent = new Intent(mActivity, CaptureActivity.class);
            intent.setAction(ACTION_SCAN);
            intent.putExtra("SCAN_MODE", "ONE_D_MODE");
            switch (signatures) {
                case engineering:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_ENG);
                    break;

                case maintenance:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAIN);
                    break;

//                case areaOwner:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_AO);
//                    break;
//
//                case mainRes:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_MAINRES);
//                    break;
//
//                case CSE:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CSE);
//                    break;

                case deptHead:
                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_DEPT);
                    break;

//                case client:
//                    mActivity.startActivityForResult(intent, REQUEST_SCAN_BARCODE_CLIENT);
//                    break;

                default:
                    break;
            }
        } catch (ActivityNotFoundException anfe) {
            showQRDialog(mActivity, "No Scanner Found",
                    "Install a scanner code application?", "Yes", "No").show();
        }
    }

    public static void saveData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        HandoverModel model = new HandoverModel();
        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        ds.insertHandOver(model);

        ds.close();
    }

    public static void updateData() {
        DataSource ds = new DataSource(mActivity);
        ds.open();

        HandoverModel model = new HandoverModel();
        model.setEquipment(equipmentET.getText().toString());
        model.setDate(dateET.getText().toString());
        model.setLocation(locationET.getText().toString());
        model.setType(typeET.getText().toString());
        model.setRegister(registerET.getText().toString());
        model.setClient(clientET.getText().toString());

        ds.updateHandOverData(model);

        ds.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        pages = PopUp.page.handover;

        if (requestCode == REQUEST_SCAN_BARCODE_ENG
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            engineeringET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.engineering;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_ENG
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

        if (requestCode == REQUEST_SCAN_BARCODE_MAIN
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            maintenanceET.setText(ds.getNameFromID(inspectorID));
            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.maintenance;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_MAIN
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

//        if (requestCode == REQUEST_SCAN_BARCODE_AO
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            areaOwnerET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.areaOwner;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_AO
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }
//
//        if (requestCode == REQUEST_SCAN_BARCODE_MAINRES
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            ugmrET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.mainRes;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_MAINRES
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }
//
//        if (requestCode == REQUEST_SCAN_BARCODE_CSE
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            cseET.setText(ds.getNameFromID(inspectorID));
//
//            String title = ds.getTitleFromID(inspectorID);
//            String name = ds.getNameFromID(inspectorID);
//
//            signatures = PopUp.signature.CSE;
//            PopUp.showInspectorPopUp(mActivity, inspectorID, name, title, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
//                    registerET.getText().toString());
//            ds.close();
//        }else if (requestCode == REQUEST_SCAN_BARCODE_CSE
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }

        if (requestCode == REQUEST_SCAN_BARCODE_DEPT
                && resultCode == mActivity.RESULT_OK) {
            String contents = data.getStringExtra("SCAN_RESULT");
            String format = data.getStringExtra("SCAN_RESULT_FORMAT");

            Toast toast = Toast.makeText(mActivity, "Content:" + contents
                    + " Format:" + format, Toast.LENGTH_LONG);
            toast.show();

            inspectorIDFalse = contents;

            DataSource ds = new DataSource(mActivity);
            ds.open();
            String[] separatorID = inspectorIDFalse.split("-");
            String inspectorID = separatorID[0];
            dataInspector = ds.getDataInspectorFromID(inspectorID);
            deptHeadET.setText(ds.getNameFromID(inspectorID));

            String name = ds.getNameFromID(inspectorID);

            if(name == "") {
                Helper.showPopUpMessage(mActivity, "Error",
                        "Your name is not registered", null);
            } else {
                signatures = PopUp.signature.deptHead;
                PopUp.showInspectorPopUp(mActivity, inspectorID, name, pages, signatures, equipmentET.getText().toString(), dateET.getText().toString(),
                        registerET.getText().toString(), null);
                ds.close();
            }
        } else if (requestCode == REQUEST_SCAN_BARCODE_DEPT
                && resultCode == mActivity.RESULT_CANCELED) {
            Helper.showPopUpMessage(mActivity, "Error",
                    "Unsupported QR Code Format", null);
        }

//        if (requestCode == REQUEST_SCAN_BARCODE_CLIENT
//                && resultCode == mActivity.RESULT_OK) {
//            String contents = data.getStringExtra("SCAN_RESULT");
//            String format = data.getStringExtra("SCAN_RESULT_FORMAT");
//
//            Toast toast = Toast.makeText(mActivity, "Content:" + contents
//                    + " Format:" + format, Toast.LENGTH_LONG);
//            toast.show();
//
//            inspectorIDFalse = contents;
//
//            DataSource ds = new DataSource(mActivity);
//            ds.open();
//            String[] separatorID = inspectorIDFalse.split("-");
//            String inspectorID = separatorID[0];
//            dataInspector = ds.getDataInspectorFromID(inspectorID);
//            clientET.setText(ds.getNameFromID(inspectorID));
//            ds.close();
//        } else if (requestCode == REQUEST_SCAN_BARCODE_CLIENT
//                && resultCode == mActivity.RESULT_CANCELED) {
//            Helper.showPopUpMessage(mActivity, "Error",
//                    "Unsupported QR Code Format", null);
//        }

        if (requestCode == PhotoPopUp.IMAGE_POP_UP_TAKE_PICTURE_CODE
                && resultCode == getActivity().RESULT_OK) {

            if (Helper.fileUri != null) {
                // new File(tempPhotoDir).mkdirs();
                Helper.setPic(PhotoPopUp.getPicture(), Helper.fileUri.getPath());
                PhotoPopUp.setImagePath(Helper.fileUri.getPath());
            }
        } else if (requestCode == PhotoPopUp.IMAGE_POP_UP_GALLERY_CODE
                && resultCode == getActivity().RESULT_OK) {

            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            PhotoPopUp.setImagePath(picturePath);
            Helper.setPic(PhotoPopUp.getPicture(), picturePath);
        }

        if (requestCode == Reports.OPEN_PREVIEW) {
            File fileName = new File(
                    FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH
                            + "/"
                            + new SimpleDateFormat("yyyyMMdd")
                            .format(new Date()) + "/"
                            + "Handover Sprinkler.pdf");
            if (fileName.exists())
                fileName.delete();
        }
    }

    public static void onBackPressed(final Activity mActivity) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
        builder.setIcon(R.drawable.warning_logo);
        builder.setTitle("Warning");
        builder.setMessage("Are you sure to exit from Commissioning Sprinkler Application?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mActivity.finish();
            }
        });

        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.setNeutralButton("SAVE AND EXIT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (validation()) {
                    saveData();
                    mActivity.finish();
                }
            }
        });

        builder.show();
    }


}
