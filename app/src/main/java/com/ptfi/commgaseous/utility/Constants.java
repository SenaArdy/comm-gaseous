package com.ptfi.commgaseous.utility;

import com.ptfi.commgaseous.model.InspectorModel;

import java.util.ArrayList;

/**
 * Created by GO on 7/18/2016.
 */
public class Constants {

    public static final String JSON_VERSION_NAME = "version_name";
    public static final String JSON_VERSION_CODE = "version_code";

    // INSPECTOR
    private static ArrayList<InspectorModel> INSPECTOR_DICTIONARYS = new ArrayList<InspectorModel>();

    public static ArrayList<InspectorModel> getInspectorDictionarys() {
        return INSPECTOR_DICTIONARYS;
    }

    public static void setInspectorDictionarys(ArrayList<InspectorModel> inspectorDictionarys) {
        INSPECTOR_DICTIONARYS = inspectorDictionarys;
    }

}
