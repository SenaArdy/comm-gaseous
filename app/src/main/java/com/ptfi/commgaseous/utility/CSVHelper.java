package com.ptfi.commgaseous.utility;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;


/**
 * Created by GO on 7/18/2016.
 */
public class CSVHelper {

    public static void appendCSV(String path,String filename, List<String[]> values)
    {
        List<String[]> previousList = CSVHelper.readCSVFromPath(path);
        previousList.addAll(values);
        CSVHelper.writeCSV(path,filename, previousList);
    }

    public static boolean writeCSV(String path,String filename,List<String[]> values)
    {
        boolean status = true;
        File f = new File(path);
        if(!(f.exists() && f.isDirectory()))
        {
            f.mkdirs();
        }

        CSVWriter writer;
        try {
            writer = new CSVWriter(new FileWriter(path+filename));
            writer.writeAll(values);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            status = false;
        }
        return status;
    }

    public static List<String[]> readCSVFromPath(String path)
    {
        List<String[]> returnValue=null;

        try {
            CSVReader reader = new CSVReader(new FileReader(path));
            if(reader != null)
            {
                returnValue = reader.readAll();
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return returnValue;

    }

    public static class Csv {
        public static String Escape(String s) {
            if (s.contains(QUOTE))
                s = s.replace(QUOTE, ESCAPED_QUOTE);

            if (indexOfAny(s, CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
                s = QUOTE + s + QUOTE;

            if (s.contains(ENTER))
                s = s.replace(ENTER, SPACE);

            return s;
        }

        public static String Unescape(String s) {
            if (s.startsWith(QUOTE) && s.endsWith(QUOTE)) {
                s = s.substring(1, s.length() - 2);

                if (s.contains(ESCAPED_QUOTE))
                    s = s.replace(ESCAPED_QUOTE, QUOTE);
            }

            return s;
        }

        private static String QUOTE = "\"";
        private static String ESCAPED_QUOTE = "\"\"";
        private static String ENTER = "\n";
        private static String SPACE = " ";
        private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };

        public static int indexOfAny(String str, char[] searchChars) {
            if (isEmpty(str) || isEmpty(searchChars)) {
                return -1;
            }
            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);
                for (int j = 0; j < searchChars.length; j++) {
                    if (searchChars[j] == ch) {
                        return i;
                    }
                }
            }
            return -1;
        }

        public static boolean isEmpty(char[] array) {
            if (array == null || array.length == 0) {
                return true;
            }
            return false;
        }

        public static boolean isEmpty(String str) {
            return str == null || str.length() == 0;
        }
    }

}
