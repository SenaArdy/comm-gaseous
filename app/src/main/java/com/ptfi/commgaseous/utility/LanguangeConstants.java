package com.ptfi.commgaseous.utility;

/**
 * Created by GO on 7/18/2016.
 */
public class LanguangeConstants {

    public String TOGGLE_BTN = "Indonesia";
    public String Q1 = "Instalasi selesai";
    public String Q2 = "Memakai bahan standar FSME";
    public String Q3 = "Test fungsional";
    public String Q4 = "Ada penggambaran";
    public String Q5 = "Kondisi sistem gaseous";
    public String Q6 = "Kepala silinder";
    public String Q7 = "Selenoid";
    public String Q8 = "Manual Aktuatori";
    public String Q9 = "Switch pembatalan";
    public String Q10 = "Switch tekanan";
    public String Q11 = "Sambungan pipa";
    public String Q12 = "Pipa semprot";
    public String Q13 = "Tanda evakuasi";
    public String Q14 = "Tanda jangan memasuki";
    public String Q15 = "Segel bangunan";
    public String Q16 = "Penanda dan penutup";

    public LanguangeConstants(boolean isEnglish) {
        if (isEnglish) {
            TOGGLE_BTN = "English";
            Q1 = "Installation complete";
            Q2 = "Material use meet FSME standard";
            Q3 = "Function test";
            Q4 = "Drawing available";
            Q5 = "Gaseous cylinders condition";
            Q6 = "Head cylinders";
            Q7 = "Solenoids";
            Q8 = "Manual actuators";
            Q9 = "Abort switches";
            Q10 = "Pressure switch";
            Q11 = "Piping connections";
            Q12 = "Nozzles";
            Q13 = "Evacuation signs";
            Q14 = "Do not enter signs";
            Q15 = "Building sealed";
            Q16 = "Tags and seals";
        } else {
            TOGGLE_BTN = "Indonesia";
            Q1 = "Instalasi selesai";
            Q2 = "Memakai bahan standar FSME";
            Q3 = "Test fungsional";
            Q4 = "Ada penggambaran";
            Q5 = "Kondisi sistem gaseous";
            Q6 = "Kepala silinder";
            Q7 = "Selenoid";
            Q8 = "Manual Aktuatori";
            Q9 = "Switch pembatalan";
            Q10 = "Switch tekanan";
            Q11 = "Sambungan pipa";
            Q12 = "Pipa semprot";
            Q13 = "Tanda evakuasi";
            Q14 = "Tanda jangan memasuki";
            Q15 = "Segel bangunan";
            Q16 = "Penanda dan penutup";
        }
    }

}
