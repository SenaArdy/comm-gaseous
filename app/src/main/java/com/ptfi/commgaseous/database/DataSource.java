package com.ptfi.commgaseous.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.SQLException;
import android.provider.ContactsContract;

import com.ptfi.commgaseous.model.ComplianceModel;
import com.ptfi.commgaseous.model.GaseousModel;
import com.ptfi.commgaseous.model.HandoverModel;
import com.ptfi.commgaseous.model.InspectorModel;
import com.ptfi.commgaseous.model.PhotoModel;
import com.ptfi.commgaseous.model.TermsModel;



import java.util.ArrayList;

/**
 * Created by GO on 7/18/2016.
 */
public class DataSource {
    private static SQLiteDatabase database;
    private DataHelper dbHelper;

    public boolean isTableExists(String tableName, boolean openDb) {
        if (openDb) {
            if (database == null || !database.isOpen()) {
                database = dbHelper.getReadableDatabase();
            }
            if (!database.isReadOnly()) {
                database.close();
                database = dbHelper.getReadableDatabase();
            }
        }
        Cursor cursor = database.rawQuery(
                "select DISTINCT tbl_name from sqlite_master where tbl_name = '"
                        + tableName + "'", null
        );
        if(cursor != null){
            if(cursor.getCount()>0){
                cursor.close();
                return true;
            }
            cursor.close();
        }
        return false;
    }

    public DataSource(Context context) {
        dbHelper = new DataHelper(context);}
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }
    public void close() {dbHelper.close();}

    /*INSPECTOR*/

    String[] allColumnsInspector = {DataHelper.INSPECTOR_NUMBER, DataHelper.INSPECTOR_NAME, DataHelper.INSPECTOR_ID,
            DataHelper.INSPECTOR_TITLE, DataHelper.INSPECTOR_ORG};

    public void deleteAllDataInspector(){
        if (isTableExists(DataHelper.INSPECTOR_TABLE, false)){
            database.delete(DataHelper.INSPECTOR_TABLE, null, null);
        }
    }
    public static long insertInspector(final InspectorModel model){
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.INSPECTOR_NAME, model.getNameIns());
        insert.put(DataHelper.INSPECTOR_ID, model.getIdIns());
        insert.put(DataHelper.INSPECTOR_TITLE, model.getTitleIns());
        insert.put(DataHelper.INSPECTOR_ORG, model.getOrgIns());

        long insertData = database.insert(DataHelper.INSPECTOR_TABLE, null, insert);
        return insertData;
    }

    public ArrayList<String[]> getAllInspector(){
        ArrayList<String[]> data = new ArrayList<String[]>();

        Cursor cursor = database.query(DataHelper.INSPECTOR_TABLE,
                allColumnsInspector, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            String[] model = new String[5];
            model[0] = cursor.getString(0);
            model[1] = cursor.getString(1);
            model[2] = cursor.getString(2);
            model[3] = cursor.getString(3);
            model[4] = cursor.getString(4);
            data.add(model);
            cursor.moveToNext();
        }
//        Make sure to close the cursor
        cursor.close();
        return data;
    }

    public ArrayList<InspectorModel> getAllDataInspector(){
        ArrayList<InspectorModel> data = new ArrayList<>();

        Cursor cursor = database.query(DataHelper.INSPECTOR_TABLE, allColumnsInspector, null, null,
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            InspectorModel model = cursorInspector(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        return data;
    }

    public ArrayList<String> getAllInspectorName(){
        ArrayList<String> data = new ArrayList<String>();

        String[] columnSelection = { DataHelper.INSPECTOR_NAME};

        Cursor cursor = database.query(DataHelper.INSPECTOR_TABLE, columnSelection,
                null, null, null, null, DataHelper.INSPECTOR_NAME, null);

        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            data.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }

    public ArrayList<InspectorModel> getDataInspectorFromID(
            String inspectorID) {
        ArrayList<InspectorModel> data = new ArrayList<>();
        // columns null means return all columns
        String query = "SELECT * FROM " + DataHelper.INSPECTOR_TABLE + " WHERE "
                + DataHelper.INSPECTOR_ID + " = '" + inspectorID + "'";
        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            InspectorModel model = cursorInspector(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public String getNameFromID(String inspectorID) {
        String nama = "";
        String query = "SELECT " + DataHelper.INSPECTOR_NAME + " FROM " + DataHelper.INSPECTOR_TABLE + " WHERE "
                + DataHelper.INSPECTOR_ID + " = '" + inspectorID + "'";
        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()) {
            nama = cursor.getString(0);
            cursor.moveToFirst();
        }

        cursor.close();
        return nama;
    }

    public String getTitleFromID(String inspectorID){
        String nama = "";
        String query = "SELECT " + DataHelper.INSPECTOR_TITLE + " FROM " + DataHelper.INSPECTOR_TABLE +
                " WHERE " + DataHelper.INSPECTOR_ID + " = '" + inspectorID + "'";
        Cursor cursor = database.rawQuery(query, null);

        cursor.moveToFirst();
        if (!cursor.isAfterLast()){
            nama = cursor.getString(0);
            cursor.moveToFirst();
        }

        cursor.close();
        return nama;
    }
    private InspectorModel cursorInspector (Cursor cursor){
        InspectorModel model = new InspectorModel();

        model.id = cursor.getLong(0);
        model.nameIns = cursor.getString(1);
        model.idIns = cursor.getString(2);
        model.titleIns = cursor.getString(3);
        model.orgIns = cursor.getString(4);

        return model;
    }

    /* == COMPLIANCE == */

    String[] allColumnCompliance = {DataHelper.COMPLIANCE_ID, DataHelper.COMPLIANCE_FINDINGS, DataHelper.COMPLIANCE_REMARK,
            DataHelper.COMPLIANCE_RESPONSIBILITY, DataHelper.COMPLIANCE_DONE, DataHelper.COMPLIANCE_EQUIPMENT,
            DataHelper.COMPLIANCE_DATE, DataHelper.COMPLIANCE_REGISTER};

    public void deleteAllDataCompliance(){
        if (isTableExists(DataHelper.COMPLIANCE_TABLE, false)) {
            database.delete(DataHelper.COMPLIANCE_TABLE, null, null);
        }
    }
    public long insertCompliance(final ComplianceModel model){
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COMPLIANCE_FINDINGS, model.getFindings());
        insert.put(DataHelper.COMPLIANCE_REMARK, model.getRemark());
        insert.put(DataHelper.COMPLIANCE_RESPONSIBILITY, model.getResponsibility());
        insert.put(DataHelper.COMPLIANCE_DONE, model.getDone());
        insert.put(DataHelper.COMPLIANCE_EQUIPMENT, model.getEquipment());
        insert.put(DataHelper.COMPLIANCE_DATE, model.getDate());
        insert.put(DataHelper.COMPLIANCE_REGISTER, model.getRegister());

        long insertData = database.insert(DataHelper.COMPLIANCE_TABLE, null, insert);

        return insertData;
    }

    public ArrayList<ComplianceModel> getAllDataCompliance(){
        ArrayList<ComplianceModel> data = new ArrayList<>();

        Cursor cursor = database.query(DataHelper.COMPLIANCE_TABLE, allColumnCompliance, null, null,
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            ComplianceModel model = cursorComplianceData(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        return data;
    }

    private ComplianceModel cursorComplianceData(Cursor cursor){
        ComplianceModel model = new ComplianceModel();

        model.id = cursor.getLong(0);
        model.findings = cursor.getString(1);
        model.remark = cursor.getString(2);
        model.responsibility = cursor.getString(3);
        model.done = cursor.getString(4);
        model.equipment = cursor.getString(5);
        model.date = cursor.getString(6);
        model.register = cursor.getString(7);

        return model;

    }

    public ArrayList<ComplianceModel> getAllComplianceEquipment(
            String equipment, String date, String register){
        ArrayList<ComplianceModel> data = new ArrayList<>();
//columns null means return to all columns
        Cursor cursor = database.query(DataHelper.COMPLIANCE_TABLE, allColumnCompliance,
                DataHelper.COMPLIANCE_EQUIPMENT + " = '" + equipment + "' AND "
                        + DataHelper.COMPLIANCE_DATE + " = '" + date + "' AND "
                        + DataHelper.COMPLIANCE_REGISTER + " = '" + register + "'" ,
                null, null, null, DataHelper.COMPLIANCE_EQUIPMENT + " *1 ASC");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            ComplianceModel model = cursorComplianceData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    /* == TERMS AND CONDITION == */
    String[] allColumnTerms = {DataHelper.TERMS_ID, DataHelper.TERMS_TERMS, DataHelper.TERMS_EQUIPMENT, DataHelper.TERMS_DATE
            , DataHelper.TERMS_REGISTER};

    public void deleteAllDataTerms(){
        if(isTableExists(DataHelper.TERMS_TABLE, false)){
            database.delete(DataHelper.TERMS_TABLE, null, null);

        }
    }

    public long insertTerms(TermsModel model){
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.TERMS_TERMS, model.getTerms());
        insert.put(DataHelper.TERMS_EQUIPMENT, model.getEquipment());
        insert.put(DataHelper.TERMS_DATE, model.getDate());
        insert.put(DataHelper.TERMS_REGISTER, model.getRegister());

        long insertData = database.insert(DataHelper.TERMS_TABLE, null, insert);

        return insertData;

    }
    public ArrayList<TermsModel> getAllDataTerms() {
        ArrayList<TermsModel> data = new ArrayList<>();

        Cursor cursor = database.query(DataHelper.TERMS_TABLE, allColumnTerms, null, null,
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            TermsModel model = cursorTermsData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        return data;
    }

    private TermsModel cursorTermsData(Cursor cursor) {
        TermsModel model = new TermsModel();

        model.id = cursor.getLong(0);
        model.terms = cursor.getString(1);
        model.equipment = cursor.getString(2);
        model.date = cursor.getString(3);
        model.register = cursor.getString(4);

        return model;
    }

    public ArrayList<TermsModel> getAllTermsEquipment(
            String equipment, String date, String register) {
        ArrayList<TermsModel> data = new ArrayList<>();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.TERMS_TABLE, allColumnTerms,
                DataHelper.TERMS_EQUIPMENT + " = '" + equipment + "' AND "
                        + DataHelper.TERMS_DATE + " = '" + date + "' AND "
                        + DataHelper.TERMS_REGISTER + " = '" + register + "'" ,
                null, null, null, DataHelper.TERMS_EQUIPMENT + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            TermsModel model = cursorTermsData(cursor);
            data.add(model);
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }
        /* == COMMISSIONING == */

    String[] allColumnsCommissioning = {DataHelper.COMMISSIONING_EQUIPMENT, DataHelper.COMMISSIONING_LOCATION, DataHelper.COMMISSIONING_DATE,
            DataHelper.COMMISSIONING_TYPE, DataHelper.COMMISSIONING_REGISTER, DataHelper.COMMISSIONING_CLIENT, DataHelper.COMMISSIONING_QUESTION1, DataHelper.COMMISSIONING_REMARK1,
            DataHelper.COMMISSIONING_QUESTION2, DataHelper.COMMISSIONING_REMARK2, DataHelper.COMMISSIONING_QUESTION3, DataHelper.COMMISSIONING_REMARK3,
            DataHelper.COMMISSIONING_QUESTION4, DataHelper.COMMISSIONING_REMARK4, DataHelper.COMMISSIONING_QUESTION5, DataHelper.COMMISSIONING_REMARK5,
            DataHelper.COMMISSIONING_QUESTION6, DataHelper.COMMISSIONING_REMARK6, DataHelper.COMMISSIONING_QUESTION7, DataHelper.COMMISSIONING_REMARK7,
            DataHelper.COMMISSIONING_QUESTION8, DataHelper.COMMISSIONING_REMARK8, DataHelper.COMMISSIONING_QUESTION9, DataHelper.COMMISSIONING_REMARK9,
            DataHelper.COMMISSIONING_QUESTION10, DataHelper.COMMISSIONING_REMARK10, DataHelper.COMMISSIONING_QUESTION11, DataHelper.COMMISSIONING_REMARK11,
            DataHelper.COMMISSIONING_QUESTION12, DataHelper.COMMISSIONING_REMARK12, DataHelper.COMMISSIONING_QUESTION13, DataHelper.COMMISSIONING_REMARK13,
            DataHelper.COMMISSIONING_QUESTION14, DataHelper.COMMISSIONING_REMARK14, DataHelper.COMMISSIONING_QUESTION15, DataHelper.COMMISSIONING_REMARK15,
            DataHelper.COMMISSIONING_QUESTION16, DataHelper.COMMISSIONING_REMARK16,
            DataHelper.COMMISSIONING_NAME_ENG, DataHelper.COMMISSIONING_ID_ENG, DataHelper.COMMISSIONING_SIGN_ENG,
            DataHelper.COMMISSIONING_NAME_MAIN, DataHelper.COMMISSIONING_ID_MAIN, DataHelper.COMMISSIONING_SIGN_MAIN,
            DataHelper.COMMISSIONING_NAME_AO, DataHelper.COMMISSIONING_ID_AO, DataHelper.COMMISSIONING_SIGN_AO,
            DataHelper.COMMISSIONING_NAME_MAINRES, DataHelper.COMMISSIONING_ID_MAINRES, DataHelper.COMMISSIONING_SIGN_MAINRES,
            DataHelper.COMMISSIONING_NAME_CSE, DataHelper.COMMISSIONING_ID_CSE, DataHelper.COMMISSIONING_SIGN_CSE,
            DataHelper.COMMISSIONING_NAME_DEPT, DataHelper.COMMISSIONING_ID_DEPT, DataHelper.COMMISSIONING_SIGN_DEPT, DataHelper.COMMISSIONING_DIVISON, DataHelper.COMMISSIONING_CONTRACTOR};

    public void deleteAllDataCommissioning() {
        if (isTableExists(DataHelper.COMMISSIONING_TABLE, false)) {
            database.delete(DataHelper.COMMISSIONING_TABLE, null, null);
        }
    }

    public long insertCommissioning(GaseousModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.COMMISSIONING_EQUIPMENT, model.getEquipment());
        insert.put(DataHelper.COMMISSIONING_LOCATION, model.getLocation());
        insert.put(DataHelper.COMMISSIONING_DATE, model.getDate());
        insert.put(DataHelper.COMMISSIONING_TYPE, model.getType());
        insert.put(DataHelper.COMMISSIONING_REGISTER, model.getRegister());
        insert.put(DataHelper.COMMISSIONING_CLIENT, model.getClient());
        insert.put(DataHelper.COMMISSIONING_QUESTION1, model.getQuestion1());
        insert.put(DataHelper.COMMISSIONING_REMARK1, model.getRemark1());
        insert.put(DataHelper.COMMISSIONING_QUESTION2, model.getQuestion2());
        insert.put(DataHelper.COMMISSIONING_REMARK2, model.getRemark2());
        insert.put(DataHelper.COMMISSIONING_QUESTION3, model.getQuestion3());
        insert.put(DataHelper.COMMISSIONING_REMARK3, model.getRemark3());
        insert.put(DataHelper.COMMISSIONING_QUESTION4, model.getQuestion4());
        insert.put(DataHelper.COMMISSIONING_REMARK4, model.getRemark4());
        insert.put(DataHelper.COMMISSIONING_QUESTION5, model.getQuestion5());
        insert.put(DataHelper.COMMISSIONING_REMARK5, model.getRemark5());
        insert.put(DataHelper.COMMISSIONING_QUESTION6, model.getQuestion6());
        insert.put(DataHelper.COMMISSIONING_REMARK6, model.getRemark6());
        insert.put(DataHelper.COMMISSIONING_QUESTION7, model.getQuestion7());
        insert.put(DataHelper.COMMISSIONING_REMARK7, model.getRemark7());
        insert.put(DataHelper.COMMISSIONING_QUESTION8, model.getQuestion8());
        insert.put(DataHelper.COMMISSIONING_REMARK8, model.getRemark8());
        insert.put(DataHelper.COMMISSIONING_QUESTION9, model.getQuestion9());
        insert.put(DataHelper.COMMISSIONING_REMARK9, model.getRemark9());
        insert.put(DataHelper.COMMISSIONING_QUESTION10, model.getQuestion10());
        insert.put(DataHelper.COMMISSIONING_REMARK10, model.getRemark10());
        insert.put(DataHelper.COMMISSIONING_QUESTION11, model.getQuestion11());
        insert.put(DataHelper.COMMISSIONING_REMARK11, model.getRemark11());
        insert.put(DataHelper.COMMISSIONING_QUESTION12, model.getQuestion12());
        insert.put(DataHelper.COMMISSIONING_REMARK12, model.getRemark12());
        insert.put(DataHelper.COMMISSIONING_QUESTION13, model.getQuestion13());
        insert.put(DataHelper.COMMISSIONING_REMARK13, model.getRemark13());
        insert.put(DataHelper.COMMISSIONING_QUESTION14, model.getQuestion14());
        insert.put(DataHelper.COMMISSIONING_REMARK14, model.getRemark14());
        insert.put(DataHelper.COMMISSIONING_QUESTION15, model.getQuestion15());
        insert.put(DataHelper.COMMISSIONING_REMARK15, model.getRemark15());
        insert.put(DataHelper.COMMISSIONING_QUESTION16, model.getQuestion16());
        insert.put(DataHelper.COMMISSIONING_REMARK16, model.getRemark16());
        insert.put(DataHelper.COMMISSIONING_NAME_ENG, model.getNameEng());
        insert.put(DataHelper.COMMISSIONING_ID_ENG, model.getIdEng());
        insert.put(DataHelper.COMMISSIONING_SIGN_ENG, model.getSignEng());
        insert.put(DataHelper.COMMISSIONING_NAME_MAIN, model.getNameMain());
        insert.put(DataHelper.COMMISSIONING_ID_MAIN, model.getIdMain());
        insert.put(DataHelper.COMMISSIONING_SIGN_MAIN, model.getSignMain());
        insert.put(DataHelper.COMMISSIONING_NAME_AO, model.getNameAO());
        insert.put(DataHelper.COMMISSIONING_ID_AO, model.getIdAO());
        insert.put(DataHelper.COMMISSIONING_SIGN_AO, model.getSignAO());
        insert.put(DataHelper.COMMISSIONING_NAME_MAINRES, model.getNameUGMR());
        insert.put(DataHelper.COMMISSIONING_ID_MAINRES, model.getIdUGMR());
        insert.put(DataHelper.COMMISSIONING_SIGN_MAINRES, model.getSignUGMR());
        insert.put(DataHelper.COMMISSIONING_NAME_CSE, model.getNameCSE());
        insert.put(DataHelper.COMMISSIONING_ID_CSE, model.getIdCSE());
        insert.put(DataHelper.COMMISSIONING_SIGN_CSE, model.getSignCSE());
        insert.put(DataHelper.COMMISSIONING_NAME_DEPT, model.getNameDept());
        insert.put(DataHelper.COMMISSIONING_ID_DEPT, model.getIdDept());
        insert.put(DataHelper.COMMISSIONING_SIGN_DEPT, model.getSignDept());
        insert.put(DataHelper.COMMISSIONING_DIVISON, model.getDivision());
        insert.put(DataHelper.COMMISSIONING_CONTRACTOR, model.getContractor());

        long insertData = database.insert(DataHelper.COMMISSIONING_TABLE, null, insert);

        return insertData;
    }


    public ArrayList<GaseousModel> getAllDataCommissioning() {
        ArrayList<GaseousModel> data = new ArrayList<>();

        Cursor cursor = database.query(DataHelper.COMMISSIONING_TABLE, allColumnsCommissioning, null, null,
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            GaseousModel model = cursorCommissioningData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        return data;
    }

    private GaseousModel cursorCommissioningData(Cursor cursor) {
        GaseousModel model = new GaseousModel();

        model.id = cursor.getLong(0);
        model.equipment = cursor.getString(1);
        model.location = cursor.getString(2);
        model.date = cursor.getString(3);
        model.type = cursor.getString(4);
        model.register = cursor.getString(5);
        model.client = cursor.getString(6);
        model.question1 = cursor.getString(7);
        model.remark1 = cursor.getString(8);
        model.question2 = cursor.getString(9);
        model.remark2 = cursor.getString(10);
        model.question3 = cursor.getString(11);
        model.remark3 = cursor.getString(12);
        model.question4 = cursor.getString(13);
        model.remark4 = cursor.getString(14);
        model.question5 = cursor.getString(15);
        model.remark5 = cursor.getString(16);
        model.question6 = cursor.getString(17);
        model.remark6 = cursor.getString(18);
        model.question7 = cursor.getString(19);
        model.remark7 = cursor.getString(20);
        model.question8 = cursor.getString(21);
        model.remark8 = cursor.getString(22);
        model.question9 = cursor.getString(23);
        model.remark9 = cursor.getString(24);
        model.question10 = cursor.getString(25);
        model.remark10 = cursor.getString(26);
        model.question11 = cursor.getString(27);
        model.remark11 = cursor.getString(28);
        model.question12 = cursor.getString(29);
        model.remark12 = cursor.getString(30);
        model.question13 = cursor.getString(31);
        model.remark13 = cursor.getString(32);
        model.question14 = cursor.getString(33);
        model.remark14 = cursor.getString(34);
        model.question15 = cursor.getString(35);
        model.remark15 = cursor.getString(36);
        model.question16 = cursor.getString(37);
        model.remark16 = cursor.getString(38);
        model.nameEng = cursor.getString(39);
        model.idEng = cursor.getString(40);
        model.signEng = cursor.getBlob(41);
        model.nameMain = cursor.getString(42);
        model.idMain = cursor.getString(43);
        model.signMain = cursor.getBlob(44);
        model.nameAO = cursor.getString(45);
        model.idAO = cursor.getString(46);
        model.signAO = cursor.getBlob(47);
        model.nameUGMR = cursor.getString(48);
        model.idUGMR = cursor.getString(49);
        model.signUGMR = cursor.getBlob(50);
        model.nameCSE = cursor.getString(51);
        model.idCSE = cursor.getString(52);
        model.signCSE = cursor.getBlob(53);
        model.nameDept = cursor.getString(54);
        model.idDept = cursor.getString(55);
        model.signDept = cursor.getBlob(56);
        model.division = cursor.getString(57);
        model.contractor = cursor.getString(58);

        return model;
    }

    public ArrayList<GaseousModel> getDataCommissioningFromDate(
            String date) {
        ArrayList<GaseousModel> data = new ArrayList<>();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.COMMISSIONING_TABLE, allColumnsCommissioning,
                DataHelper.COMMISSIONING_DATE + " = '" + date + "'",
                null, null, null, DataHelper.COMMISSIONING_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            GaseousModel model = cursorCommissioningData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public ArrayList<GaseousModel> getDataCommissioningFromEquipment(
            String equipment) {
        ArrayList<GaseousModel> data = new ArrayList<>();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.COMMISSIONING_TABLE, allColumnsCommissioning,
                DataHelper.COMMISSIONING_EQUIPMENT + " = '" + equipment + "'",
                null, null, null, DataHelper.COMMISSIONING_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            GaseousModel model = cursorCommissioningData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public ArrayList<GaseousModel> getDataCommissioningFromEquipmentAndDate(
            String equipment, String date) {
        ArrayList<GaseousModel> data = new ArrayList<>();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.COMMISSIONING_TABLE, allColumnsCommissioning,
                DataHelper.COMMISSIONING_EQUIPMENT + " = '" + equipment + "' AND "
                        + DataHelper.COMMISSIONING_DATE + " = '" + date + "'",
                null, null, null, DataHelper.COMMISSIONING_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            GaseousModel model = cursorCommissioningData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public void updateCommissioningData(final ContentValues insert, final GaseousModel model) {
        insert.put(DataHelper.COMMISSIONING_LOCATION, model.getLocation());
        insert.put(DataHelper.COMMISSIONING_TYPE, model.getType());
        insert.put(DataHelper.COMMISSIONING_CLIENT, model.getClient());
        insert.put(DataHelper.COMMISSIONING_QUESTION1, model.getQuestion1());
        insert.put(DataHelper.COMMISSIONING_REMARK1, model.getRemark1());
        insert.put(DataHelper.COMMISSIONING_QUESTION2, model.getQuestion2());
        insert.put(DataHelper.COMMISSIONING_REMARK2, model.getRemark2());
        insert.put(DataHelper.COMMISSIONING_QUESTION3, model.getQuestion3());
        insert.put(DataHelper.COMMISSIONING_REMARK3, model.getRemark3());
        insert.put(DataHelper.COMMISSIONING_QUESTION4, model.getQuestion4());
        insert.put(DataHelper.COMMISSIONING_REMARK4, model.getRemark4());
        insert.put(DataHelper.COMMISSIONING_QUESTION5, model.getQuestion5());
        insert.put(DataHelper.COMMISSIONING_REMARK5, model.getRemark5());
        insert.put(DataHelper.COMMISSIONING_QUESTION6, model.getQuestion6());
        insert.put(DataHelper.COMMISSIONING_REMARK6, model.getRemark6());
        insert.put(DataHelper.COMMISSIONING_QUESTION7, model.getQuestion7());
        insert.put(DataHelper.COMMISSIONING_REMARK7, model.getRemark7());
        insert.put(DataHelper.COMMISSIONING_QUESTION8, model.getQuestion8());
        insert.put(DataHelper.COMMISSIONING_REMARK8, model.getRemark8());
        insert.put(DataHelper.COMMISSIONING_QUESTION9, model.getQuestion9());
        insert.put(DataHelper.COMMISSIONING_REMARK9, model.getRemark9());
        insert.put(DataHelper.COMMISSIONING_QUESTION10, model.getQuestion10());
        insert.put(DataHelper.COMMISSIONING_REMARK10, model.getRemark10());
        insert.put(DataHelper.COMMISSIONING_QUESTION11, model.getQuestion11());
        insert.put(DataHelper.COMMISSIONING_REMARK11, model.getRemark11());
        insert.put(DataHelper.COMMISSIONING_QUESTION12, model.getQuestion12());
        insert.put(DataHelper.COMMISSIONING_REMARK12, model.getRemark12());
        insert.put(DataHelper.COMMISSIONING_QUESTION13, model.getQuestion13());
        insert.put(DataHelper.COMMISSIONING_REMARK13, model.getRemark13());
        insert.put(DataHelper.COMMISSIONING_QUESTION14, model.getQuestion14());
        insert.put(DataHelper.COMMISSIONING_REMARK14, model.getRemark14());
        insert.put(DataHelper.COMMISSIONING_QUESTION15, model.getQuestion15());
        insert.put(DataHelper.COMMISSIONING_REMARK15, model.getRemark15());
        insert.put(DataHelper.COMMISSIONING_QUESTION16, model.getQuestion16());
        insert.put(DataHelper.COMMISSIONING_REMARK16, model.getRemark16());
    }

    public long updateCommissioningData (final GaseousModel model) {
        ContentValues insert = new ContentValues();
        updateCommissioningData(insert, model);
        long createID = 0;
        database.update(DataHelper.COMMISSIONING_TABLE, insert, DataHelper.COMMISSIONING_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.COMMISSIONING_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.COMMISSIONING_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateDeptCommissioning(final ContentValues insert, final GaseousModel model) {
        insert.put(DataHelper.COMMISSIONING_NAME_DEPT, model.getNameDept());
        insert.put(DataHelper.COMMISSIONING_ID_DEPT, model.getIdDept());
        insert.put(DataHelper.COMMISSIONING_SIGN_DEPT, model.getSignDept());
    }

    public long updateDeptCommissioning (final GaseousModel model) {
        ContentValues insert = new ContentValues();
        updateDeptCommissioning(insert, model);
        long createID = 0;
        database.update(DataHelper.COMMISSIONING_TABLE, insert, DataHelper.COMMISSIONING_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.COMMISSIONING_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.COMMISSIONING_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateCSECommissioning(final ContentValues insert, final GaseousModel model) {
        insert.put(DataHelper.COMMISSIONING_NAME_CSE, model.getNameCSE());
        insert.put(DataHelper.COMMISSIONING_ID_CSE, model.getIdCSE());
        insert.put(DataHelper.COMMISSIONING_SIGN_CSE, model.getSignCSE());
    }

    public long updateCSECommissioning (final GaseousModel model) {
        ContentValues insert = new ContentValues();
        updateCSECommissioning(insert, model);
        long createID = 0;
        database.update(DataHelper.COMMISSIONING_TABLE, insert, DataHelper.COMMISSIONING_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.COMMISSIONING_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.COMMISSIONING_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateMainResCommissioning(final ContentValues insert, final GaseousModel model) {
        insert.put(DataHelper.COMMISSIONING_NAME_MAINRES, model.getNameUGMR());
        insert.put(DataHelper.COMMISSIONING_ID_MAINRES, model.getIdUGMR());
        insert.put(DataHelper.COMMISSIONING_SIGN_MAINRES, model.getSignUGMR());
    }

    public long updateMainResCommissioning (final GaseousModel model) {
        ContentValues insert = new ContentValues();
        updateMainResCommissioning(insert, model);
        long createID = 0;
        database.update(DataHelper.COMMISSIONING_TABLE, insert, DataHelper.COMMISSIONING_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.COMMISSIONING_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.COMMISSIONING_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateAOCommissioning(final ContentValues insert, final GaseousModel model) {
        insert.put(DataHelper.COMMISSIONING_NAME_AO, model.getNameAO());
        insert.put(DataHelper.COMMISSIONING_ID_AO, model.getIdAO());
        insert.put(DataHelper.COMMISSIONING_SIGN_AO, model.getSignAO());
    }

    public long updateAOCommissioning (final GaseousModel model) {
        ContentValues insert = new ContentValues();
        updateAOCommissioning(insert, model);
        long createID = 0;
        database.update(DataHelper.COMMISSIONING_TABLE, insert, DataHelper.COMMISSIONING_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.COMMISSIONING_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.COMMISSIONING_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateMainCommissioning(final ContentValues insert, final GaseousModel model) {
        insert.put(DataHelper.COMMISSIONING_NAME_MAIN, model.getNameMain());
        insert.put(DataHelper.COMMISSIONING_ID_MAIN, model.getIdMain());
        insert.put(DataHelper.COMMISSIONING_SIGN_MAIN, model.getSignMain());
    }

    public long updateMainCommissioning (final GaseousModel model) {
        ContentValues insert = new ContentValues();
        updateMainCommissioning(insert, model);
        long createID = 0;
        database.update(DataHelper.COMMISSIONING_TABLE, insert, DataHelper.COMMISSIONING_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.COMMISSIONING_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.COMMISSIONING_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateEngCommissioning(final ContentValues insert, final GaseousModel model) {
        insert.put(DataHelper.COMMISSIONING_NAME_ENG, model.getNameEng());
        insert.put(DataHelper.COMMISSIONING_ID_ENG, model.getIdEng());
        insert.put(DataHelper.COMMISSIONING_SIGN_ENG, model.getSignEng());
    }

    public long updateEngCommissioning (final GaseousModel model) {
        ContentValues insert = new ContentValues();
        updateEngCommissioning(insert, model);
        long createID = 0;
        database.update(DataHelper.COMMISSIONING_TABLE, insert, DataHelper.COMMISSIONING_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.COMMISSIONING_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.COMMISSIONING_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public GaseousModel getDataCommissioning(String equipment, String date,
                                             String location, String type, String register) {
        GaseousModel data = new GaseousModel();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.COMMISSIONING_TABLE, allColumnsCommissioning,
                DataHelper.COMMISSIONING_EQUIPMENT + " = '" + equipment + "' AND " +
                        DataHelper.COMMISSIONING_DATE + " = '" + date + "' AND " +
                        DataHelper.COMMISSIONING_LOCATION + " = '" + location + "' AND " +
                        DataHelper.COMMISSIONING_TYPE + " = '" + type + "' AND " +
                        DataHelper.COMMISSIONING_REGISTER + " = '" + register + "'",
                null, null, null, DataHelper.COMMISSIONING_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            GaseousModel model = cursorCommissioningData(cursor);
            data = model;
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    /* ============================
                               PHOTO's
    ============================ */
    String[] allColoumnPhotos = {DataHelper.PHOTO_ID, DataHelper.PHOTO_PATH, DataHelper.PHOTO_TITLE, DataHelper.PHOTO_COMMENT,
            DataHelper.PHOTO_EQUIPMENT, DataHelper.PHOTO_DATE, DataHelper.PHOTO_REGISTER};

    public void deleteAllPhotos() {
        if (isTableExists(DataHelper.PHOTO_TABLE, false)) {
            database.delete(DataHelper.PHOTO_TABLE, null, null);
        }
    }

    public long insertPhotos(PhotoModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.PHOTO_PATH, model.getPathFile());
        insert.put(DataHelper.PHOTO_TITLE, model.getTitle());
        insert.put(DataHelper.PHOTO_COMMENT, model.getComment());
        insert.put(DataHelper.PHOTO_EQUIPMENT, model.getEquipment());
        insert.put(DataHelper.PHOTO_DATE, model.getDate());
        insert.put(DataHelper.PHOTO_REGISTER, model.getRegister());

        long insertData = database.insert(DataHelper.PHOTO_TABLE, null, insert);

        return insertData;
    }

    public ArrayList<PhotoModel> getAllPhotos() {
        ArrayList<PhotoModel> data = new ArrayList<>();

        Cursor cursor = database.query(DataHelper.PHOTO_TABLE, allColoumnPhotos, null, null,
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PhotoModel model = cursorPhotoData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        return data;
    }

    private PhotoModel cursorPhotoData(Cursor cursor) {
        PhotoModel model = new PhotoModel();

        model.id = cursor.getLong(0);
        model.pathFile = cursor.getString(1);
        model.title = cursor.getString(2);
        model.comment = cursor.getString(3);
        model.equipment = cursor.getString(4);
        model.date = cursor.getString(5);
        model.register = cursor.getString(6);

        return model;
    }

    public ArrayList<PhotoModel> getAllPhotoData(
            String equipment, String date, String register) {
        ArrayList<PhotoModel> data = new ArrayList<>();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.PHOTO_TABLE, allColoumnPhotos,
                DataHelper.PHOTO_EQUIPMENT + " = '" + equipment + "' AND "
                        + DataHelper.PHOTO_DATE + " = '" + date + "' AND "
                        + DataHelper.PHOTO_REGISTER + " = '" + register + "'" ,
                null, null, null, DataHelper.PHOTO_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            PhotoModel model = cursorPhotoData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public void updatePhotoData(final ContentValues insert, final PhotoModel model) {
        insert.put(DataHelper.PHOTO_PATH, model.getPathFile());
        insert.put(DataHelper.PHOTO_TITLE, model.getTitle());
        insert.put(DataHelper.PHOTO_COMMENT, model.getComment());
        insert.put(DataHelper.PHOTO_EQUIPMENT, model.getEquipment());
        insert.put(DataHelper.PHOTO_DATE, model.getDate());
        insert.put(DataHelper.PHOTO_REGISTER, model.getRegister());
    }

    public long updatePhotoData (final PhotoModel model, final String path, final String title, final String comment) {
        ContentValues insert = new ContentValues();
        updatePhotoData(insert, model);
        long createID = 0;
        database.update(DataHelper.PHOTO_TABLE, insert, DataHelper.PHOTO_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.PHOTO_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.PHOTO_REGISTER + " = '" + model.getRegister() + "' AND "
                + DataHelper.PHOTO_PATH + " = '" + path + "' AND "
                + DataHelper.PHOTO_TITLE + " = '" + title + "' AND "
                + DataHelper.PHOTO_COMMENT + " = '" + comment + "'", null);

        return createID;
    }

    public boolean validatePhotoData(final String equipment, final String date, final String register,
                                     final String path, final String title, final String comment ) {
        String query = "SELECT * FROM " + DataHelper.PHOTO_TABLE + " WHERE "
                + DataHelper.PHOTO_EQUIPMENT + " = '" + equipment + "' AND "
                + DataHelper.PHOTO_DATE + " = '" + date + "' AND "
                + DataHelper.PHOTO_REGISTER + " = '" + register + "' AND "
                + DataHelper.PHOTO_PATH + " = '" + path + "' AND "
                + DataHelper.PHOTO_TITLE + " = '" + title + "' AND "
                + DataHelper.PHOTO_COMMENT + " = '" + comment + "'";
        Cursor cursor = database.rawQuery(query, null);
        if(cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();

        return true;
    }

    /* ========================
              HANDOVER
    ======================== */
    String[] allColoumnHandOver = {DataHelper.HANDOVER_ID, DataHelper.HANDOVER_EQUIPMENT, DataHelper.HANDOVER_LOCATION, DataHelper.HANDOVER_DATE,
            DataHelper.HANDOVER_TYPE, DataHelper.HANDOVER_REGISTER, DataHelper.HANDOVER_CLIENT, DataHelper.HANDOVER_NAME_ENG, DataHelper.HANDOVER_ID_ENG,
            DataHelper.HANDOVER_SIGN_ENG, DataHelper.HANDOVER_NAME_MAIN, DataHelper.HANDOVER_ID_MAIN, DataHelper.HANDOVER_SIGN_MAIN,
            DataHelper.HANDOVER_NAME_AO, DataHelper.HANDOVER_ID_AO, DataHelper.HANDOVER_SIGN_AO, DataHelper.HANDOVER_NAME_MAINRES,
            DataHelper.HANDOVER_ID_MAINRES, DataHelper.HANDOVER_SIGN_MAINRES, DataHelper.HANDOVER_NAME_CSE, DataHelper.HANDOVER_ID_CSE,
            DataHelper.HANDOVER_SIGN_CSE, DataHelper.HANDOVER_NAME_DEPT, DataHelper.HANDOVER_ID_DEPT, DataHelper.HANDOVER_SIGN_DEPT,
            DataHelper.COMMISSIONING_DIVISON, DataHelper.COMMISSIONING_CONTRACTOR};

    public void deleteAllHandOver() {
        if (isTableExists(DataHelper.HANDOVER_TABLE, false)) {
            database.delete(DataHelper.HANDOVER_TABLE, null, null);
        }
    }

    public long insertHandOver(HandoverModel model) {
        ContentValues insert = new ContentValues();

        insert.put(DataHelper.HANDOVER_EQUIPMENT, model.getEquipment());
        insert.put(DataHelper.HANDOVER_LOCATION, model.getLocation());
        insert.put(DataHelper.HANDOVER_DATE, model.getDate());
        insert.put(DataHelper.HANDOVER_TYPE, model.getType());
        insert.put(DataHelper.HANDOVER_REGISTER, model.getRegister());
        insert.put(DataHelper.HANDOVER_CLIENT, model.getClient());
        insert.put(DataHelper.HANDOVER_NAME_ENG, model.getNameEng());
        insert.put(DataHelper.HANDOVER_ID_ENG, model.getIdEng());
        insert.put(DataHelper.HANDOVER_SIGN_ENG, model.getSignEng());
        insert.put(DataHelper.HANDOVER_NAME_MAIN, model.getNameMain());
        insert.put(DataHelper.HANDOVER_ID_MAIN, model.getIdMain());
        insert.put(DataHelper.HANDOVER_SIGN_MAIN, model.getSignMain());
        insert.put(DataHelper.HANDOVER_NAME_AO, model.getNameAO());
        insert.put(DataHelper.HANDOVER_ID_AO, model.getIdAO());
        insert.put(DataHelper.HANDOVER_SIGN_AO, model.getSignAO());
        insert.put(DataHelper.HANDOVER_NAME_MAINRES, model.getNameUGMR());
        insert.put(DataHelper.HANDOVER_ID_MAINRES, model.getIdUGMR());
        insert.put(DataHelper.HANDOVER_SIGN_MAINRES, model.getSignUGMR());
        insert.put(DataHelper.HANDOVER_NAME_CSE, model.getNameCSE());
        insert.put(DataHelper.HANDOVER_ID_CSE, model.getIdCSE());
        insert.put(DataHelper.HANDOVER_SIGN_CSE, model.getSignCSE());
        insert.put(DataHelper.HANDOVER_NAME_DEPT, model.getNameDept());
        insert.put(DataHelper.HANDOVER_ID_DEPT, model.getIdDept());
        insert.put(DataHelper.HANDOVER_SIGN_DEPT, model.getSignDept());
        insert.put(DataHelper.COMMISSIONING_DIVISON, model.getDivision());
        insert.put(DataHelper.COMMISSIONING_CONTRACTOR, model.getContractor());

        long insertData = database.insert(DataHelper.HANDOVER_TABLE, null, insert);

        return insertData;
    }

    public ArrayList<HandoverModel> getAllDataHandOver() {
        ArrayList<HandoverModel> data = new ArrayList<>();

        Cursor cursor = database.query(DataHelper.HANDOVER_TABLE, allColoumnHandOver, null, null,
                null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HandoverModel model = cursorHandOverData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        return data;
    }

    private HandoverModel cursorHandOverData(Cursor cursor) {
        HandoverModel model = new HandoverModel();

        model.id = cursor.getLong(0);
        model.equipment = cursor.getString(1);
        model.location = cursor.getString(2);
        model.date = cursor.getString(3);
        model.type = cursor.getString(4);
        model.register = cursor.getString(5);
        model.client = cursor.getString(6);
        model.nameEng = cursor.getString(7);
        model.idEng = cursor.getString(8);
        model.signEng = cursor.getBlob(9);
        model.nameMain = cursor.getString(10);
        model.idMain = cursor.getString(11);
        model.signMain = cursor.getBlob(12);
        model.nameAO = cursor.getString(13);
        model.idAO = cursor.getString(14);
        model.signAO = cursor.getBlob(15);
        model.nameUGMR = cursor.getString(16);
        model.idUGMR = cursor.getString(17);
        model.signUGMR = cursor.getBlob(18);
        model.nameCSE = cursor.getString(19);
        model.idCSE = cursor.getString(20);
        model.signCSE = cursor.getBlob(21);
        model.nameDept = cursor.getString(22);
        model.idDept = cursor.getString(23);
        model.signDept = cursor.getBlob(24);
        model.division = cursor.getString(25);
        model.contractor = cursor.getString(26);

        return model;
    }

    public ArrayList<HandoverModel> getDataHandOverFromDate(
            String date) {
        ArrayList<HandoverModel> data = new ArrayList<>();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.HANDOVER_TABLE, allColoumnHandOver,
                DataHelper.HANDOVER_DATE + " = '" + date + "'",
                null, null, null, DataHelper.HANDOVER_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HandoverModel model = cursorHandOverData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public ArrayList<HandoverModel> getDataHandOverFromEquipment(
            String equipment) {
        ArrayList<HandoverModel> data = new ArrayList<>();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.HANDOVER_TABLE, allColoumnHandOver,
                DataHelper.HANDOVER_EQUIPMENT + " = '" + equipment + "'",
                null, null, null, DataHelper.HANDOVER_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HandoverModel model = cursorHandOverData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public ArrayList<HandoverModel> getDataHandOverFromEquipmentAndDate(
            String equipment, String date) {
        ArrayList<HandoverModel> data = new ArrayList<>();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.HANDOVER_TABLE, allColoumnHandOver,
                DataHelper.HANDOVER_EQUIPMENT + " = '" + equipment + "' AND "
                        + DataHelper.HANDOVER_DATE + " = '" + date + "'",
                null, null, null, DataHelper.HANDOVER_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HandoverModel model = cursorHandOverData(cursor);
            data.add(model);
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public void updateHandOverData(final ContentValues insert, final HandoverModel model) {
        insert.put(DataHelper.HANDOVER_LOCATION, model.getLocation());
        insert.put(DataHelper.HANDOVER_TYPE, model.getType());
        insert.put(DataHelper.HANDOVER_CLIENT, model.getClient());
    }

    public long updateHandOverData (final HandoverModel model) {
        ContentValues insert = new ContentValues();
        updateHandOverData(insert, model);
        long createID = 0;
        database.update(DataHelper.HANDOVER_TABLE, insert, DataHelper.HANDOVER_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.HANDOVER_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.HANDOVER_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateEngHandOver(final ContentValues insert, final HandoverModel model) {
        insert.put(DataHelper.HANDOVER_NAME_ENG, model.getNameEng());
        insert.put(DataHelper.HANDOVER_ID_ENG, model.getIdEng());
        insert.put(DataHelper.HANDOVER_SIGN_ENG, model.getSignEng());
    }

    public long updateEngHandOver (final HandoverModel model) {
        ContentValues insert = new ContentValues();
        updateEngHandOver(insert, model);
        long createID = 0;
        database.update(DataHelper.HANDOVER_TABLE, insert, DataHelper.HANDOVER_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.HANDOVER_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.HANDOVER_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateMainHandOver(final ContentValues insert, final HandoverModel model) {
        insert.put(DataHelper.HANDOVER_NAME_MAIN, model.getNameMain());
        insert.put(DataHelper.HANDOVER_ID_MAIN, model.getIdMain());
        insert.put(DataHelper.HANDOVER_SIGN_MAIN, model.getSignMain());
    }

    public long updateMainHandOver (final HandoverModel model) {
        ContentValues insert = new ContentValues();
        updateMainHandOver(insert, model);
        long createID = 0;
        database.update(DataHelper.HANDOVER_TABLE, insert, DataHelper.HANDOVER_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.HANDOVER_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.HANDOVER_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateAOHandOver(final ContentValues insert, final HandoverModel model) {
        insert.put(DataHelper.HANDOVER_NAME_AO, model.getNameAO());
        insert.put(DataHelper.HANDOVER_ID_AO, model.getIdAO());
        insert.put(DataHelper.HANDOVER_SIGN_AO, model.getSignAO());
    }

    public long updateAOHandOver (final HandoverModel model) {
        ContentValues insert = new ContentValues();
        updateAOHandOver(insert, model);
        long createID = 0;
        database.update(DataHelper.HANDOVER_TABLE, insert, DataHelper.HANDOVER_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.HANDOVER_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.HANDOVER_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateMainResHandOver(final ContentValues insert, final HandoverModel model) {
        insert.put(DataHelper.HANDOVER_NAME_MAINRES, model.getNameUGMR());
        insert.put(DataHelper.HANDOVER_ID_MAINRES, model.getIdUGMR());
        insert.put(DataHelper.HANDOVER_SIGN_MAINRES, model.getSignUGMR());
    }

    public long updateMainResHandOver (final HandoverModel model) {
        ContentValues insert = new ContentValues();
        updateMainResHandOver(insert, model);
        long createID = 0;
        database.update(DataHelper.HANDOVER_TABLE, insert, DataHelper.HANDOVER_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.HANDOVER_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.HANDOVER_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateCSEHandOver(final ContentValues insert, final HandoverModel model) {
        insert.put(DataHelper.HANDOVER_NAME_CSE, model.getNameCSE());
        insert.put(DataHelper.HANDOVER_ID_CSE, model.getIdCSE());
        insert.put(DataHelper.HANDOVER_SIGN_CSE, model.getSignCSE());
    }

    public long updateCSEHandOver (final HandoverModel model) {
        ContentValues insert = new ContentValues();
        updateCSEHandOver(insert, model);
        long createID = 0;
        database.update(DataHelper.HANDOVER_TABLE, insert, DataHelper.HANDOVER_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.HANDOVER_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.HANDOVER_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public void updateDeptHandOver(final ContentValues insert, final HandoverModel model) {
        insert.put(DataHelper.HANDOVER_NAME_DEPT, model.getNameDept());
        insert.put(DataHelper.HANDOVER_ID_DEPT, model.getIdDept());
        insert.put(DataHelper.HANDOVER_SIGN_DEPT, model.getSignDept());
    }

    public long updateDeptHandOver (final HandoverModel model) {
        ContentValues insert = new ContentValues();
        updateDeptHandOver(insert, model);
        long createID = 0;
        database.update(DataHelper.HANDOVER_TABLE, insert, DataHelper.HANDOVER_EQUIPMENT
                + " = '" + model.getEquipment() + "' AND "
                + DataHelper.HANDOVER_DATE + " = '" + model.getDate() + "' AND "
                + DataHelper.HANDOVER_REGISTER + " = '" + model.getRegister() + "'", null);

        return createID;
    }

    public HandoverModel getDataHandOver(String equipment, String date,
                                         String location, String type, String register) {
        HandoverModel data = new HandoverModel();
        // columns null means return all columns
        Cursor cursor = database.query(DataHelper.HANDOVER_TABLE, allColumnsCommissioning,
                DataHelper.HANDOVER_EQUIPMENT + " = '" + equipment + "' AND " +
                        DataHelper.HANDOVER_DATE + " = '" + date + "' AND " +
                        DataHelper.HANDOVER_LOCATION + " = '" + location + "' AND " +
                        DataHelper.HANDOVER_TYPE + " = '" + type + "' AND " +
                        DataHelper.HANDOVER_REGISTER + " = '" + register + "'",
                null, null, null, DataHelper.HANDOVER_ID + " *1 ASC ");

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            HandoverModel model = cursorHandOverData(cursor);
            data = model;
            cursor.moveToNext();
        }

        cursor.close();
        return data;
    }

    public boolean validateDataCommissioning(final String equipment, final String date, final String register) {
        String query = "SELECT * FROM " + DataHelper.COMMISSIONING_TABLE + " WHERE "
                + DataHelper.COMMISSIONING_EQUIPMENT + " = '" + equipment + "' AND "
                + DataHelper.COMMISSIONING_DATE + " = '" + date + "' AND "
                + DataHelper.COMMISSIONING_REGISTER + " = '" + register + "'";
        Cursor cursor = database.rawQuery(query, null);
        if(cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();

        return true;
    }


    public boolean validateDataHandOver(final String equipment, final String date, final String register) {
        String query = "SELECT * FROM " + DataHelper.HANDOVER_TABLE + " WHERE "
                + DataHelper.HANDOVER_EQUIPMENT + " = '" + equipment + "' AND "
                + DataHelper.HANDOVER_DATE + " = '" + date + "' AND "
                + DataHelper.HANDOVER_REGISTER + " = '" + register + "'";
        Cursor cursor = database.rawQuery(query, null);
        if(cursor.getCount() <= 0) {
            cursor.close();
            return false;
        }
        cursor.close();

        return true;
    }

}

