package com.ptfi.commgaseous;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ptfi.commgaseous.database.DataSource;
import com.ptfi.commgaseous.model.InspectorModel;
import com.ptfi.commgaseous.utility.CSVHelper;
import com.ptfi.commgaseous.utility.Constants;
import com.ptfi.commgaseous.utility.FoldersFilesName;
import com.ptfi.commgaseous.utility.Helper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by GO on 7/18/2016.
 */

public class SplashScreen extends Activity {

    Activity mActivity = SplashScreen.this;
    static ImageView logo;
    private static int SPLASH_TIME_OUT = 4000;

    LinearLayout agreeSplashScreen;
    TextView activityText;
    ProgressBar progressBar1;

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splashscreen);

        agreeSplashScreen = (LinearLayout) findViewById(R.id.agreeSplashScreen);
        agreeSplashScreen.setVisibility(View.INVISIBLE);
        progressBar1 = (ProgressBar) findViewById(R.id.progressBar1);

        setActivityStatus("Initialize Data");
        startAnimations();
        initializeApplication();
    }
    private void initializeApplication() {
        new AsyncTask<String, Integer, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Void doInBackground(String... params) {
                initVersionFile();
                initAllFolders();
                initTemplateDocuments();
//                Helper.initLicense(mActivity);

                // FILE NAME PATH DEFINITION
                File importManpower = new File(FoldersFilesName.BASEPATH + "/"
                        + FoldersFilesName.APP_FOLDER_NAME + "/"
                        + FoldersFilesName.IMPORT_FOLDER_NAME + "/"
                        + FoldersFilesName.INSPECTOR_FILE_NAME);

                DataSource dataSource = new DataSource(mActivity);
                dataSource.open();

                // IMPORT DATA TO DATABASE INSPECTOR
                if (dataSource.getAllDataInspector().isEmpty()) {
                    File importFolderChecked = new File(FoldersFilesName.BASEPATH + "/"
                            + FoldersFilesName.APP_FOLDER_NAME + "/"
                            + FoldersFilesName.IMPORT_FOLDER_NAME + "/"
                            + FoldersFilesName.INSPECTOR_FILE_NAME);
                    if (!importFolderChecked.exists()) {
                        Helper.copyFileAsset(FoldersFilesName.INSPECTOR_FILE_NAME,
                                FoldersFilesName.IMPORT_FOLDER_NAME, SplashScreen.this);
                    }
                }

                if (importManpower.exists()) {
                    if (dataSource.getAllDataInspector().isEmpty()) {
                        dataSource.deleteAllDataInspector();
                        String pathPanel = Environment.getExternalStorageDirectory()
                                .getAbsolutePath();
                        pathPanel += "/" + FoldersFilesName.ROOT_FOLDER_NAME + "/"
                                + FoldersFilesName.APP_FOLDER_NAME + "/"
                                + FoldersFilesName.IMPORT_FOLDER_NAME + "/"
                                + FoldersFilesName.INSPECTOR_FILE_NAME;

                        List<String[]> readResultPanel = CSVHelper
                                .readCSVFromPath(pathPanel);

                        int i = 0;

                        for (String[] strings : readResultPanel) {
                            if (i > 0 && strings.length > 1) {
                                InspectorModel om = new InspectorModel();
                                om.setNameIns(strings[0]);
                                om.setIdIns(strings[1]);
                                om.setTitleIns(strings[2]);
                                om.setOrgIns(strings[2]);

                                dataSource.insertInspector(om);
                            }
                            i++;
                        }
                        importManpower.delete();
                    }
                }
                Constants.setInspectorDictionarys(dataSource
                        .getAllDataInspector());

                try {
                    Thread.sleep(SPLASH_TIME_OUT);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                dataSource.close();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressBar1.setVisibility(View.INVISIBLE);
                activityText.setVisibility(View.INVISIBLE);
                agreeSplashScreen.setVisibility(View.VISIBLE);
                agreeSplashScreen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mActivity, MainMenu.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }.execute();
    }

    private void startAnimations() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);

        anim = AnimationUtils.loadAnimation(this, R.anim.animation);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.logo);
        iv.clearAnimation();
        iv.startAnimation(anim);
    }

    private void setActivityStatus(final String text) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if (text != null) {
                    activityText = (TextView) findViewById(R.id.activityText);
                    if (activityText != null) {
                        activityText.setText(text);
                    }
                }
            }
        });
    }

    private void initVersionFile() {
        setActivityStatus("Checking Application Version");

        Helper.createDirectory(FoldersFilesName.EXTERNAL_ROOT_FOLDER);

        if (isFolderOutdated()) {
            try {
                Helper.deleteAllFilesOnDirectory(FoldersFilesName.DB_FOLDER_ON_EXTERNAL_PATH);
            } catch (Exception e) {
                e.printStackTrace();
            }
            createVersionFile();
        }
    }

    public boolean isFolderOutdated() {
        String fileContent = readTextFile(FoldersFilesName.EXTERNAL_ROOT_FOLDER
                + "/" + FoldersFilesName.VERSION_FILE);

        try {
            JSONObject json = new JSONObject(fileContent);

            int version = json.getInt(Constants.JSON_VERSION_CODE);
            if (version < Helper.getAppVersionCode(mActivity)) {
                return true;
            } else {
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return true;
        }
    }

    public void createVersionFile() {
        File file = new File(FoldersFilesName.EXTERNAL_ROOT_FOLDER + "/"
                + FoldersFilesName.VERSION_FILE);

        try {
            FileWriter fWriter = new FileWriter(file);
            fWriter.write(createVersionObj().toString());
            fWriter.flush();
            fWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public JSONObject createVersionObj() throws JSONException {
        JSONObject versionObj = new JSONObject();

        versionObj.put(Constants.JSON_VERSION_CODE,
                Helper.getAppVersionCode(mActivity));
        versionObj.put(Constants.JSON_VERSION_NAME, Helper.getAppVersion(mActivity));

        return versionObj;
    }

    public String readTextFile(String path) {
        String result = "";

        File file = new File(path);
        if (file.exists()) {
            // Read text from file
            StringBuilder text = new StringBuilder();

            try {
                BufferedReader br = new BufferedReader(new FileReader(file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
                br.close();

                result = text.toString();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }
        }
        return result;
    }

    private void initAllFolders() {
        setActivityStatus("Create Application Folders");

        Helper.createDirectory(FoldersFilesName.DB_FOLDER_ON_EXTERNAL_PATH);
        Helper.createDirectory(FoldersFilesName.IMPORT_FOLDER_ON_EXTERNAL_PATH);
        Helper.createDirectory(FoldersFilesName.EXPORT_FOLDER_ON_EXTERNAL_PATH);
        Helper.createDirectory(FoldersFilesName.TEMPLATES_FOLDER_ON_EXTERNAL_PATH);
    }

    private void initTemplateDocuments() {
        setActivityStatus("Initialize Template Documents");

        Helper.copyFolderAndContent(
                FoldersFilesName.TEMPLATES_FOLDER_ON_EXTERNAL_PATH,
                FoldersFilesName.TEMPLATES_FOLDER_ON_ASSETS_PATH, mActivity);

    }
}
