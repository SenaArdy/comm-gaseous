package com.ptfi.commgaseous.model;

/**
 * Created by GO on 7/18/2016.
 */
public class GaseousModel {

    public long id;
    public String equipment;
    public String location;
    public String client;
    public String date;
    public String type;
    public String register;
    public String question1;
    public String remark1;
    public String question2;
    public String remark2;
    public String question3;
    public String remark3;
    public String question4;
    public String remark4;
    public String question5;
    public String remark5;
    public String question6;
    public String remark6;
    public String question7;
    public String remark7;
    public String question8;
    public String remark8;
    public String question9;
    public String remark9;
    public String question10;
    public String remark10;
    public String question11;
    public String remark11;
    public String question12;
    public String remark12;
    public String question13;
    public String remark13;
    public String question14;
    public String remark14;
    public String question15;
    public String remark15;
    public String question16;
    public String remark16;
    public String nameEng;
    public String idEng;
    public byte[] signEng;
    public String nameMain;
    public String idMain;
    public byte[] signMain;
    public String nameAO;
    public String idAO;
    public byte[] signAO;
    public String nameUGMR;
    public String idUGMR;
    public byte[] signUGMR;
    public String nameCSE;
    public String idCSE;
    public byte[] signCSE;
    public String nameDept;
    public String idDept;
    public byte[] signDept;
    public String division;
    public String contractor;

    public GaseousModel() {
        this.id = -1;
        this.equipment = "";
        this.location = "";
        this.client = "";
        this.date = "";
        this.type = "";
        this.register = "";
        this.question1 = "";
        this.remark1 = "";
        this.question2 = "";
        this.remark2 = "";
        this.question3 = "";
        this.remark3 = "";
        this.question4 = "";
        this.remark4 = "";
        this.question5 = "";
        this.remark5 = "";
        this.question6 = "";
        this.remark6 = "";
        this.question7 = "";
        this.remark7 = "";
        this.question8 = "";
        this.remark8 = "";
        this.question9 = "";
        this.remark9 = "";
        this.question10 = "";
        this.remark10 = "";
        this.question11 = "";
        this.remark11 = "";
        this.question12 = "";
        this.remark12 = "";
        this.question13 = "";
        this.remark13 = "";
        this.question14 = "";
        this.remark14 = "";
        this.question15 = "";
        this.remark15 = "";
        this.question16 = "";
        this.remark16 = "";
        this.nameEng = "";
        this.idEng = "";
        this.signEng = null;
        this.nameMain = "";
        this.idMain = "";
        this.signMain = null;
        this.nameAO = "";
        this.idAO = "";
        this.signAO = null;
        this.nameUGMR = "";
        this.idUGMR = "";
        this.signUGMR = null;
        this.nameCSE = "";
        this.idCSE = "";
        this.signCSE = null;
        this.nameDept = "";
        this.idDept = "";
        this.signDept = null;
        this.division = "";
        this.contractor = "";
    }

    public GaseousModel(long id, String equipment, String location, String client, String date, String type, String register, String question1, String remark1,
                          String question2, String remark2, String question3, String remark3, String question4, String remark4, String question5, String remark5, String question6,
                          String remark6, String question7, String remark7, String question8, String remark8, String question9, String remark9, String question10, String remark10,
                          String question11, String remark11, String question12, String remark12, String question13, String remark13, String question14, String remark14,
                          String question15, String remark15, String question16, String remark16, String nameEng, String idEng, byte[] signEng, String nameMain, String idMain,
                          byte[] signMain, String nameAO, String idAO, byte[] signAO, String nameUGMR, String idUGMR, byte[] signUGMR, String nameCSE, String idCSE, byte[] signCSE,
                          String nameDept, String idDept, byte[] signDept, String division, String contractor) {
        this.id = id;
        this.equipment = equipment;
        this.location = location;
        this.client = client;
        this.date = date;
        this.type = type;
        this.register = register;
        this.question1 = question1;
        this.remark1 = remark1;
        this.question2 = question2;
        this.remark2 = remark2;
        this.question3 = question3;
        this.remark3 = remark3;
        this.question4 = question4;
        this.remark4 = remark4;
        this.question5 = question5;
        this.remark5 = remark5;
        this.question6 = question6;
        this.remark6 = remark6;
        this.question7 = question7;
        this.remark7 = remark7;
        this.question8 = question8;
        this.remark8 = remark8;
        this.question9 = question9;
        this.remark9 = remark9;
        this.question10 = question10;
        this.remark10 = remark10;
        this.question11 = question11;
        this.remark11 = remark11;
        this.question12 = question12;
        this.remark12 = remark12;
        this.question13 = question13;
        this.remark13 = remark13;
        this.question14 = question14;
        this.remark14 = remark14;
        this.question15 = question15;
        this.remark15 = remark15;
        this.question16 = question16;
        this.remark16 = remark16;
        this.nameEng = nameEng;
        this.idEng = idEng;
        this.signEng = signEng;
        this.nameMain = nameMain;
        this.idMain = idMain;
        this.signMain = signMain;
        this.nameAO = nameAO;
        this.idAO = idAO;
        this.signAO = signAO;
        this.nameUGMR = nameUGMR;
        this.idUGMR = idUGMR;
        this.signUGMR = signUGMR;
        this.nameCSE = nameCSE;
        this.idCSE = idCSE;
        this.signCSE = signCSE;
        this.nameDept = nameDept;
        this.idDept = idDept;
        this.signDept = signDept;
        this.division = division;
        this.contractor = contractor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getIdEng() {
        return idEng;
    }

    public void setIdEng(String idEng) {
        this.idEng = idEng;
    }



    public String getNameMain() {
        return nameMain;
    }

    public void setNameMain(String nameMain) {
        this.nameMain = nameMain;
    }

    public String getIdMain() {
        return idMain;
    }

    public void setIdMain(String idMain) {
        this.idMain = idMain;
    }



    public String getNameAO() {
        return nameAO;
    }

    public void setNameAO(String nameAO) {
        this.nameAO = nameAO;
    }

    public String getIdAO() {
        return idAO;
    }

    public void setIdAO(String idAO) {
        this.idAO = idAO;
    }



    public String getNameUGMR() {
        return nameUGMR;
    }

    public void setNameUGMR(String nameUGMR) {
        this.nameUGMR = nameUGMR;
    }

    public String getIdUGMR() {
        return idUGMR;
    }

    public void setIdUGMR(String idUGMR) {
        this.idUGMR = idUGMR;
    }



    public String getNameCSE() {
        return nameCSE;
    }

    public void setNameCSE(String nameCSE) {
        this.nameCSE = nameCSE;
    }

    public String getIdCSE() {
        return idCSE;
    }

    public void setIdCSE(String idCSE) {
        this.idCSE = idCSE;
    }



    public String getNameDept() {
        return nameDept;
    }

    public void setNameDept(String nameDept) {
        this.nameDept = nameDept;
    }

    public String getIdDept() {
        return idDept;
    }

    public void setIdDept(String idDept) {
        this.idDept = idDept;
    }

    public byte[] getSignEng() {
        return signEng;
    }

    public void setSignEng(byte[] signEng) {
        this.signEng = signEng;
    }

    public byte[] getSignMain() {
        return signMain;
    }

    public void setSignMain(byte[] signMain) {
        this.signMain = signMain;
    }

    public byte[] getSignAO() {
        return signAO;
    }

    public void setSignAO(byte[] signAO) {
        this.signAO = signAO;
    }

    public byte[] getSignUGMR() {
        return signUGMR;
    }

    public void setSignUGMR(byte[] signUGMR) {
        this.signUGMR = signUGMR;
    }

    public byte[] getSignCSE() {
        return signCSE;
    }

    public void setSignCSE(byte[] signCSE) {
        this.signCSE = signCSE;
    }

    public byte[] getSignDept() {
        return signDept;
    }

    public void setSignDept(byte[] signDept) {
        this.signDept = signDept;
    }

    public String getQuestion1() {
        return question1;
    }

    public void setQuestion1(String question1) {
        this.question1 = question1;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1;
    }

    public String getQuestion2() {
        return question2;
    }

    public void setQuestion2(String question2) {
        this.question2 = question2;
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2;
    }

    public String getQuestion3() {
        return question3;
    }

    public void setQuestion3(String question3) {
        this.question3 = question3;
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3;
    }

    public String getQuestion4() {
        return question4;
    }

    public void setQuestion4(String question4) {
        this.question4 = question4;
    }

    public String getRemark4() {
        return remark4;
    }

    public void setRemark4(String remark4) {
        this.remark4 = remark4;
    }

    public String getQuestion5() {
        return question5;
    }

    public void setQuestion5(String question5) {
        this.question5 = question5;
    }

    public String getRemark5() {
        return remark5;
    }

    public void setRemark5(String remark5) {
        this.remark5 = remark5;
    }

    public String getQuestion6() {
        return question6;
    }

    public void setQuestion6(String question6) {
        this.question6 = question6;
    }

    public String getRemark6() {
        return remark6;
    }

    public void setRemark6(String remark6) {
        this.remark6 = remark6;
    }

    public String getQuestion7() {
        return question7;
    }

    public void setQuestion7(String question7) {
        this.question7 = question7;
    }

    public String getRemark7() {
        return remark7;
    }

    public void setRemark7(String remark7) {
        this.remark7 = remark7;
    }

    public String getQuestion8() {
        return question8;
    }

    public void setQuestion8(String question8) {
        this.question8 = question8;
    }

    public String getRemark8() {
        return remark8;
    }

    public void setRemark8(String remark8) {
        this.remark8 = remark8;
    }

    public String getQuestion9() {
        return question9;
    }

    public void setQuestion9(String question9) {
        this.question9 = question9;
    }

    public String getRemark9() {
        return remark9;
    }

    public void setRemark9(String remark9) {
        this.remark9 = remark9;
    }

    public String getQuestion10() {
        return question10;
    }

    public void setQuestion10(String question10) {
        this.question10 = question10;
    }

    public String getRemark10() {
        return remark10;
    }

    public void setRemark10(String remark10) {
        this.remark10 = remark10;
    }

    public String getQuestion11() {
        return question11;
    }

    public void setQuestion11(String question11) {
        this.question11 = question11;
    }

    public String getRemark11() {
        return remark11;
    }

    public void setRemark11(String remark11) {
        this.remark11 = remark11;
    }

    public String getQuestion12() {
        return question12;
    }

    public void setQuestion12(String question12) {
        this.question12 = question12;
    }

    public String getRemark12() {
        return remark12;
    }

    public void setRemark12(String remark12) {
        this.remark12 = remark12;
    }

    public String getQuestion13() {
        return question13;
    }

    public void setQuestion13(String question13) {
        this.question13 = question13;
    }

    public String getRemark13() {
        return remark13;
    }

    public void setRemark13(String remark13) {
        this.remark13 = remark13;
    }

    public String getQuestion14() {
        return question14;
    }

    public void setQuestion14(String question14) {
        this.question14 = question14;
    }

    public String getRemark14() {
        return remark14;
    }

    public void setRemark14(String remark14) {
        this.remark14 = remark14;
    }

    public String getQuestion15() {
        return question15;
    }

    public void setQuestion15(String question15) {
        this.question15 = question15;
    }

    public String getRemark15() {
        return remark15;
    }

    public void setRemark15(String remark15) {
        this.remark15 = remark15;
    }

    public String getQuestion16() {
        return question16;
    }

    public void setQuestion16(String question16) {
        this.question16 = question16;
    }

    public String getRemark16() {
        return remark16;
    }

    public void setRemark16(String remark16) {
        this.remark16 = remark16;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public String getContractor() {
        return contractor;
    }

    public void setContractor(String contractor) {
        this.contractor = contractor;
    }
}
