package com.ptfi.commgaseous.model;

/**
 * Created by GO on 7/18/2016.
 */
public class PhotoModel {

    public long id;
    public String pathFile;
    public String title;
    public String comment;
    public String equipment;
    public String date;
    public String register;

    public PhotoModel() {
        this.id = -1;
        this.pathFile = "";
        this.title = "";
        this.comment = "";
        this.equipment = "";
        this.date = "";
        this.register = "";
    }

    public PhotoModel(long id, String pathFile, String title, String comment, String equipment, String date, String register) {
        this.id = id;
        this.pathFile = pathFile;
        this.title = title;
        this.comment = comment;
        this.equipment = equipment;
        this.date = date;
        this.register = register;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRegister() {
        return register;
    }

    public void setRegister(String register) {
        this.register = register;
    }
}
