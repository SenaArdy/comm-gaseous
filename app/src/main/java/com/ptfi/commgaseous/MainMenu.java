package com.ptfi.commgaseous;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ptfi.commgaseous.fragment.QuickContactFragment;
import com.ptfi.commgaseous.utility.Helper;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by GO on 7/19/2016.
 */
    public class MainMenu extends AppCompatActivity {

        Activity mActivity = MainMenu.this;
        CardView inspectionCV, sheInspectionCV;

        @InjectView(R.id.toolbar)
        Toolbar toolbar;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.fragment_mainmenu);

            ButterKnife.inject(MainMenu.this);
            setSupportActionBar(toolbar);

            initComponent();
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            customActionbar();
            return true;
        }

        private void customActionbar() {
            LayoutInflater customBar = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View actionBarView = customBar.inflate(R.layout.activity_all_pages, null);
            ActionBar actionbar = getSupportActionBar();
            actionbar.setDisplayHomeAsUpEnabled(false);
            actionbar.setDisplayShowHomeEnabled(false);
            actionbar.setDisplayShowCustomEnabled(true);
            actionbar.setDisplayShowTitleEnabled(false);
            actionbar.setBackgroundDrawable(new ColorDrawable(Color.BLACK));
            actionbar.setCustomView(actionBarView);
        }
// TO-DO onOptionsItemSelected to being fixed after running the essential main menu function
        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_contact2:
                    QuickContactFragment.newInstance().show(getFragmentManager(), "QuickContactFragment");
                    return true;

                case R.id.action_contact:
                    QuickContactFragment.newInstance().show(getFragmentManager(), "QuickContactFragment");
                    return true;
            }
            return super.onOptionsItemSelected(item);
        }

        public void aboutChangelog(View v) {
            QuickContactFragment.newInstance().show(getFragmentManager(), "QuickContactFragment");
        }

        public void initComponent() {
            inspectionCV = (CardView) findViewById(R.id.inspectionCV);
            sheInspectionCV = (CardView) findViewById(R.id.sheInspectionCV);

            inspectionCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, PagerSlidingMenu.class);
                    startActivity(intent);
                    finish();
                }
            });

            sheInspectionCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity, R.style.AppCompatAlertDialogStyle);
                    builder.setIcon(R.drawable.warning_logo);
                    builder.setTitle("Open SHE Inspection Application?");
                    builder.setMessage("This app will be closed before you launch SHE Application");

                    builder.setPositiveButton("AGREE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.dios.she",
                                    "com.dios.she.MainActivity"));
                            if (Helper.isAvailableIntent(MainMenu.this,
                                    intent)) {
                                intent.putExtra("isOtherApp", true);
                                startActivity(intent);
                            } else {
                                Helper.showPopUpMessage(
                                                MainMenu.this,
                                                "Warning",
                                                "SHE Inspection application not found, please install SHE Inspection application",
                                                null);
                            }
                        }
                    });

                    builder.setNegativeButton("DISAGREE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    builder.show();
                }
            });
        }


    }
//}
